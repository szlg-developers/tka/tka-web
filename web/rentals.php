<?php
	global $conn;

	if(!empty($_GET['student_id']))
	{
		/*----------  Egy adott diákhoz tartozó kölcsönzések - Adatok lekérése  ----------*/

		// Létezik-e ez a diák? Plusz diák adatainak lekérése
		$query = $conn->prepare('
			SELECT `students`.`name`, `classes`.`id`, `classes`.`name`, COUNT(`rentals`.`book_code`) FROM `students`
			INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
			LEFT JOIN `rentals` ON `rentals`.`student_id` = `students`.`id`
			WHERE `classes`.`school_id` = ? AND `students`.`id` = ?
			GROUP BY `students`.`id`, `students`.`name`, `classes`.`id`, `classes`.`name`
		');
		$query->bind_param('ii', $GLOBALS['SCHOOL_ID'], $_GET['student_id']);
		$query->execute();
		$query->bind_result($student_name, $class_id, $class_name, $rental_db);
		if(!$query->fetch())
		{
			$query->close();
			unset($_GET['student_id']);
		}
		else
		{
			$query->close();

			// Hozzáfűzés az oldal címéhez
			$plus_title = ' ('.$student_name.')';

			// Diák kölcsönzéseinek lekérése
			$rentals = [];
			$query = $conn->prepare('
				SELECT `booktypes`.`id`, `booktypes`.`title`, `books`.`code`, `rentals`.`date` FROM `rentals`
				INNER JOIN `books` ON `books`.`code` = `rentals`.`book_code`
				INNER JOIN `booktypes` ON `booktypes`.`id` = `books`.`type_id`
				INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
				WHERE `bookcategories`.`school_id` = ? AND `rentals`.`student_id` = ?
				ORDER BY `booktypes`.`title`, `booktypes`.`id`
			');
			$query->bind_param('ii', $GLOBALS['SCHOOL_ID'], $_GET['student_id']);
			$query->execute();
			$query->bind_result($type_id, $title, $code, $date);
			$act_type = 0;
			while(($success = $query->fetch()) || $act_type)
			{
				if($act_type == $type_id && $success)
				{
					$act_rental['codes'][] = array
					(
						'code' => $code,
						'date' => $date,
					);
				}
				else
				{
					if($act_type)
					{
						$rentals[] = $act_rental;
					}
					if($success)
					{
						$act_rental = array
						(
							'title' => $title,
							'codes' => [],
						);
						$act_rental['codes'][] = array
						(
							'code' => $code,
							'date' => $date,
						);
					}
					$act_type = $success ? $type_id : 0;
				}
			}
			$query->close();
		}
	}
	else if(!empty($_GET['booktype_id']))
	{
		/*----------  Egy adott típusú könyvhöz tartozó kölcsönzések - Adatok lekérése  ----------*/

		// Létezik-e ez a könyvtípus? Plusz könyvtípus adatainak lekérése
		$query = $conn->prepare('
			SELECT `booktypes`.`title`, COUNT(`books`.`code`), COUNT(`rentals`.`student_id`) FROM `booktypes`
			INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
			LEFT JOIN `books` ON `books`.`type_id` = `booktypes`.`id`
			LEFT JOIN `rentals` ON `rentals`.`book_code` = `books`.`code`
			WHERE `bookcategories`.`school_id` = ? AND `booktypes`.`id` = ?
			GROUP BY `booktypes`.`id`, `booktypes`.`title`
		');
		$query->bind_param('ii', $GLOBALS['SCHOOL_ID'], $_GET['booktype_id']);
		$query->execute();
		$query->bind_result($book_title, $book_db, $rental_db);
		if(!$query->fetch())
		{
			$query->close();
			unset($_GET['booktype_id']);
		}
		else
		{
			$query->close();

			$plus_title = ' ('.$book_title.')';

			// Az adott könyvtípushoz tartozó könyvek lekérése
			$books = [];
			$query = $conn->prepare('
				SELECT `books`.`code`, `rentals`.`date`, `students`.`id`, `students`.`name`, `classes`.`id`, `classes`.`name` FROM `books`
				INNER JOIN `booktypes` ON `booktypes`.`id` = `books`.`type_id`
				INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
				LEFT JOIN `rentals` ON `rentals`.`book_code` = `books`.`code`
				LEFT JOIN `students` ON `students`.`id` = `rentals`.`student_id`
				LEFT JOIN `classes` ON `classes`.`id` = `students`.`class_id`
				WHERE `bookcategories`.`school_id` = ? AND `books`.`type_id` = ?
			');
			$query->bind_param('ii', $GLOBALS['SCHOOL_ID'], $_GET['booktype_id']);
			$query->execute();
			$query->bind_result($code, $date, $student_id, $student_name, $class_id, $class_name);
			while($query->fetch())
			{
				$books[] = array
				(
					'code' => $code,
					'date' => $date,
					'student_id' => $student_id,
					'student_name' => $student_name,
					'class_id' => $class_id,
					'class_name' => $class_name,
				);
			}
			$query->close();
		}
	}
	else
	{
		/*----------  Összes kölcsönzés listázása diákonként - Adatok lekérése  ----------*/

		// Kölcsönzések lekérése
		$students = [];
		$result = $conn->query('
			SELECT `students`.`id`, `students`.`name`, `classes`.`id` AS `class_id`, `classes`.`name` AS `class`, COUNT(`rentals`.`book_code`) AS `rental_db` FROM `students`
			INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
			LEFT JOIN `rentals` ON `rentals`.`student_id` = `students`.`id`
			WHERE `classes`.`school_id` = '.$GLOBALS['SCHOOL_ID'].'
			GROUP BY `students`.`id`, `students`.`name`, `classes`.`id`, `classes`.`name`
			HAVING `rental_db` > 0
			ORDER BY `students`.`name`, `classes`.`name`
		');
		while($row = $result->fetch_assoc())
		{
			$students[] = array
			(
				'id' => $row['id'],
				'name' => $row['name'],
				'class_id' => $row['class_id'],
				'class' => $row['class'],
				'rental_db' => $row['rental_db'],
			);
		}
		$result->close();
	}

	$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Kölcsönzések<?php echo isset($plus_title) ? $plus_title : '' ?> - <?php echo $GLOBALS['SCHOOL_NAME'] ?> - TKA</title>
	<?php include 'includes/head.php'; ?>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/myurlap.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/rentals.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/myurlap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/rentals.css">
</head>
<body>
	<div id="main-container">
		<?php include 'includes/header.php'; ?>
		<?php if(!empty($_GET['student_id'])): ?>
			<!-- Egy adott diákhoz tartozó kölcsönzések -->
			<div id="student_header">
				<h2 id="student_title" class="title"><?php echo htmlspecialchars($student_name) ?> (<a href="osztalyok?class_id=<?php echo $class_id ?>"><?php echo htmlspecialchars($class_name) ?></a>) - <?php echo $rental_db ?> kölcsönzés</h2>
				<button id="student_add_rental" onclick="add_student_rental(this, <?php echo intval($_GET['student_id']) ?>);">Új kölcsönzés</button>
			</div>
			<div id="student_rentals" class="container">
				<?php foreach ($rentals as $rental): ?>
					<div class="row<?php echo $_SESSION['user']['level'] == 1 ? ' level-diak' : '' ?>">
						<p class="ellipsis_text" title="<?php echo htmlspecialchars($rental['title']) ?>"><?php echo htmlspecialchars($rental['title']) ?></p>
						<div class="codes">
							<?php foreach ($rental['codes'] as $code): ?>
								<div class="code">
									<p><?php echo htmlspecialchars($code['code']) ?></p>
									<p><?php echo $code['date'] ?></p>
									<button onclick="del_rental(this, '<?php echo htmlspecialchars($code['code']) ?>');">Visszahozta</button>
								</div>
							<?php endforeach ?>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		<?php elseif(!empty($_GET['booktype_id'])): ?>
			<!-- Egy adott típusú könyvhöz tartozó kölcsönzések -->
			<h2 id="book_title"><?php echo htmlspecialchars($book_title) ?> (<?php echo $rental_db ?>/<?php echo $book_db ?> kikölcsönözve)</h2>
			<div id="book_rentals" class="container">
				<?php foreach ($books as $book): ?>
					<div class="row">
						<p><?php echo htmlspecialchars($book['code']) ?></p>
						<?php if($book['date']): ?>
							<p><a href="kolcsonzesek?student_id=<?php echo $book['student_id'] ?>"><?php echo htmlspecialchars($book['student_name']) ?></a> (<a href="osztalyok?class_id=<?php echo $book['class_id'] ?>"><?php echo htmlspecialchars($book['class_name']) ?></a>) - <?php echo $book['date'] ?></p>
							<?php if ($_SESSION['user']['level'] > 1): ?>
								<button onclick="del_rental(this, '<?php echo $book['code'] ?>');">Visszahozta</button>
							<?php endif ?>
						<?php else: ?>
							<p>-</p>
							<button onclick="add_book_rental(this, '<?php echo $book['code'] ?>');">Kikölcsönzés</button>
						<?php endif ?>
					</div>
				<?php endforeach ?>
			</div>
		<?php else: ?>
			<!-- Összes kölcsönzés listázása diákonként -->
			<h2 id="rentals_title" class="title">Diákok kölcsönzései ABC rendben</h2>
			<div id="rentals" class="container">
				<?php foreach ($students as $student): ?>
					<div class="row">
						<p><?php echo htmlspecialchars($student['name']) ?> (<a href="osztalyok?class_id=<?php echo $student['class_id'] ?>"><?php echo $student['class'] ?></a>)</p>
						<p><?php echo $student['rental_db'] ?> kikölcsönzött könyv</p>
						<a href="kolcsonzesek?student_id=<?php echo $student['id'] ?>">Bővebben >></a>
					</div>
				<?php endforeach ?>
			</div>
		<?php endif; ?>
		<?php include 'includes/footer.php' ?>
	</div>
</body>
</html>
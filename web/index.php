<?php

require '../config.php';
session_start();

function init_mysqli()
{
	global $conn;
	$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, 'tka');
}

function login_check(int $level = 0, bool $strict = false, bool $redirect = false)
{
	global $URL;

	if(empty($_SESSION['user']))
		return !$redirect ? false : redirect_url('bejelentkezes?continue='.urlencode($URL));

	if($strict)
	{
		global $conn;

		$result = $conn->query('
			SELECT `users`.`username`, `users`.`name`, `users`.`password`, `users`.`level`, `schools`.`code` AS `school` FROM `users`
			LEFT JOIN `schools` ON `schools`.`id` = `users`.`school_id`
			WHERE `users`.`id` = '.$_SESSION['user']['id'].'
		');
		$user_row = $result->fetch_assoc();
		if($result->num_rows == 0 || $user_row['username'] != $_SESSION['user']['username'] || $user_row['name'] != $_SESSION['user']['name'] || $user_row['password'] != $_SESSION['user']['password'] || $user_row['level'] != $_SESSION['user']['level'] || $user_row['school'] != $_SESSION['user']['school'])
		{
			$result->close();
			unset($_SESSION['user']);
			return !$redirect ? false : redirect_url('bejelentkezes?continue='.urlencode($URL));
		}
		$result->close();
	}

	if($level > $_SESSION['user']['level'])
		return !$redirect ? false : load_page('403.php');

	return true;
}

function load_page(string $path)
{
	if(empty($_SESSION['csrf']))
		$_SESSION['csrf'] = bin2hex(random_bytes(32));
	require $path;
	exit;
}
function redirect_url(string $location)
{
	header('Location: '.WEB_ROOT.'/'.$location);
	exit;
}

global $URL;
$URL = empty($_GET['url']) ? '' : $_GET['url'];

switch($URL)
{
	case '':
		if(login_check())
		{
			if(login_check(4))
				redirect_url('rendszergazda/');
			redirect_url($_SESSION['user']['school'].'/');
		}
		redirect_url('bejelentkezes');
	case 'bejelentkezes':
		init_mysqli();
		if(login_check(0, true))
			redirect_url('./');
		load_page('login.php');
	default:
		$url_parts = explode('/', $URL);
		if($url_parts[0] == 'rendszergazda')
		{
			init_mysqli();
			login_check(4, true, true);
			if(empty($url_parts[1]))
				redirect_url('rendszergazda/iskolak');
			switch($url_parts[1])
			{
				case 'iskolak':
					load_page('schools.php');
				case 'hozzaferesek':
					load_page('sysadmin_access.php');
				default:
					load_page('404.php');
			}
		}
		login_check(1, false, true);
		if(!login_check(4) && $url_parts[0] != $_SESSION['user']['school'])
			load_page('403.php');
		init_mysqli();
		$query = $conn->prepare('SELECT `id`, `name` FROM `schools` WHERE `code` = ?');
		$query->bind_param('s', $url_parts[0]);
		$query->execute();
		$query->bind_result($school_id, $school_name);
		if($query->fetch())
		{
			$query->close();
			if(empty($url_parts[1]))
				redirect_url($url_parts[0].'/kolcsonzesek');
			$GLOBALS['SCHOOL'] = $url_parts[0];
			$GLOBALS['SCHOOL_ID'] = $school_id;
			$GLOBALS['SCHOOL_NAME'] = $school_name;
			switch($url_parts[1])
			{
				case 'kolcsonzesek':
					login_check(1, true, true);
					load_page('rentals.php');
				case 'konyvek':
					login_check(1, true, true);
					load_page('books.php');
				case 'osztalyok':
					login_check(1, true, true);
					load_page('students.php');
				case 'hozzaferesek':
					login_check(3, true, true);
					load_page('access.php');
				default:
					load_page('404.php');
			}
		}
		$query->close();
		load_page('404.php');
}

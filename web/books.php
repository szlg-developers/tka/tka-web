<?php
	global $conn;

	// könyvkategóriák lekérése
	$categories = [];
	$result = $conn->query('
		SELECT `bookcategories`.`id`, `bookcategories`.`name`, COUNT(DISTINCT `booktypes`.`id`) AS `booktype_db`, COUNT(`books`.`code`) AS `book_db` FROM `bookcategories`
		LEFT JOIN `booktypes` ON `booktypes`.`category_id` = `bookcategories`.`id`
		LEFT JOIN `books` ON `books`.`type_id` = `booktypes`.`id`
		WHERE `bookcategories`.`school_id` = '.$GLOBALS['SCHOOL_ID'].'
		GROUP BY `bookcategories`.`id`, `bookcategories`.`name`
	');
	while($row = $result->fetch_assoc())
	{
		$categories[] = array
		(
			'id' => $row['id'],
			'name' => $row['name'],
			'booktype_db' => $row['booktype_db'],
			'book_db' => $row['book_db'],
		);
	}
	$result->close();

	$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Könyvek - <?php echo $GLOBALS['SCHOOL_NAME'] ?> - TKA</title>
	<?php include 'includes/head.php'; ?>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/myurlap.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/books.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/myurlap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/books.css">
</head>
<body>
	<div id="main-container">
		<?php include 'includes/header.php'; ?>
		<div id="title">
			<h2>Könyvek</h2>
			<?php if ($_SESSION['user']['level'] > 1): ?>
				<button onclick="add_category(this);">Kategória hozzáadása</button>
				<button onclick="add_type(this);">Könyv hozzáadása</button>
				<button onclick="import_books(this);">Könyvek importálása</button>
			<?php endif ?>
		</div>
		<?php foreach ($categories as $category): ?>
			<div class="category">
				<div class="title" title="Lenyitás" data-title-open="Lenyitás" data-title-loading="Betöltés..." data-title-close="Becsukás" onclick="if(event.target.nodeName != 'BUTTON') show_types(this, <?php echo $category['id'] ?>);">
					<p><?php echo htmlspecialchars($category['name']) ?> (<?php echo $category['booktype_db'] ?> könyvtípus, <?php echo $category['book_db'] ?> könyv)</p>
					<?php if ($_SESSION['user']['level'] > 1): ?>
						<button onclick="edit_category(this, <?php echo $category['id'] ?>);">Szerkesztés</button>
						<button onclick="del_category(this, <?php echo $category['id'] ?>);">Törlés</button>
					<?php endif ?>
				</div>
				<div class="types" style="display: none;">
					<!-- <div class="type">
						<p>11. Matematika - Mozaik (7 összesen, 4 kikölcsönözve)</p>
						<?php if ($_SESSION['user']['level'] > 1): ?>
							<button onclick="edit_type(this, 1);">Szerkesztés</button>
							<button onclick="del_type(this, 1);">Törlés</button>
						<?php endif ?>
						<a href="rentals.php?booktype_id=1">Kölcsönzések >></a>
					</div> -->
				</div>
				<img src="<?php echo WEB_ROOT ?>/assets/img/loading.gif" style="display: none;">
			</div>
		<?php endforeach ?>
		<?php include 'includes/footer.php' ?>
	</div>
</body>
</html>
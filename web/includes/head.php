<meta charset="utf-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
	<link rel="icon" type="icon" href="<?php echo WEB_ROOT ?>/favicon.ico">
<?php if (!empty($_SESSION['user'])): ?>
	<!-- Requirements -->
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/functions.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/mymessage.css">
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/main.css">
	<!-- CSRF token -->
	<script type="text/javascript">const csrf = '<?php echo $_SESSION['csrf'] ?>';</script>
	<!-- myfetch limits and webroot -->
	<script type="text/javascript">const myfetch_limits = { 'post_max_size': '<?php echo ini_get('post_max_size') ?>', 'upload_max_filesize': '<?php echo ini_get('upload_max_filesize') ?>', 'max_file_uploads': '<?php echo ini_get('max_file_uploads') ?>' }; const myfetch_webroot = '<?php echo WEB_ROOT ?>';</script>
	<!-- School ID -->
	<script type="text/javascript">const school_id = <?php echo empty($GLOBALS['SCHOOL_ID']) ? 1 : $GLOBALS['SCHOOL_ID'] ?>;</script>
<?php endif ?>

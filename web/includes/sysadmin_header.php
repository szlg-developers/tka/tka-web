<div id="header">
		<div id="header_menu">
			<a href="iskolak" title="Iskolák"<?php echo 'rendszergazda/iskolak' == $GLOBALS['URL'] ? ' class="active"' : ''; ?>>Iskolák</a>
			<a href="hozzaferesek" title="Hozzáférések"<?php echo 'rendszergazda/hozzaferesek' == $GLOBALS['URL'] ? ' class="active"' : ''; ?>>Hozzáférések</a>
		</div>
		<div id="header_profile">
			<h3><?php echo $_SESSION['user']['name'] ?></h3>
			<form method="POST" action="<?php echo ROOT_URL ?>/backend/logout.php">
				<input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf'] ?>">
				<a href="javascript:;" onclick="this.parentNode.submit();" title="Kijelentkezés">Kijelentkezés</a>
			</form>
		</div>
	</div>

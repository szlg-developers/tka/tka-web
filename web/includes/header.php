<div id="header">
			<div id="header_menu">
				<a href="kolcsonzesek" title="Kölcsönzések"<?php echo $GLOBALS['SCHOOL'].'/kolcsonzesek' == $GLOBALS['URL'] ? ' class="active"' : ''; ?>>Kölcsönzések</a>
				<a href="konyvek" title="Könyvek"<?php echo $GLOBALS['SCHOOL'].'/konyvek' == $GLOBALS['URL'] ? ' class="active"' : ''; ?>>Könyvek</a>
				<a href="osztalyok" title="Osztályok"<?php echo $GLOBALS['SCHOOL'].'/osztalyok' == $GLOBALS['URL'] ? ' class="active"' : ''; ?>>Osztályok</a>
				<?php if ($_SESSION['user']['level'] > 2): ?>
					<a href="hozzaferesek" title="Hozzáférések"<?php echo $GLOBALS['SCHOOL'].'/hozzaferesek' == $GLOBALS['URL'] ? ' class="active"' : ''; ?>>Hozzáférések</a>
				<?php endif ?>
				<?php if ($_SESSION['user']['level'] == 4): ?>
					<a href="<?php echo WEB_ROOT ?>/rendszergazda/" title="Vissza a rendszergazdai felületre" class="ellipsis_text">Vissza a rendszergazdai felületre</a>
				<?php endif ?>
			</div>
			<div id="header_profile">
				<h3><?php echo $_SESSION['user']['name'] ?></h3>
				<form method="POST" action="<?php echo WEB_ROOT ?>/backend/logout.php">
					<input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf'] ?>">
					<a href="javascript:;" onclick="this.parentNode.submit();" title="Kijelentkezés">Kijelentkezés</a>
				</form>
			</div>
		</div>

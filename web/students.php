<?php
	global $conn;

	if(!empty($_GET['class_id']))
	{
		/*----------  Osztály névsorának listázása - Adatok lekérése  ----------*/

		// Létezik-e az osztály? Osztály adatainak lekérése
		$query = $conn->prepare('SELECT `name` FROM `classes` WHERE `school_id` = ? AND `id` = ?');
		$query->bind_param('ii', $GLOBALS['SCHOOL_ID'], $_GET['class_id']);
		$query->execute();
		$query->bind_result($class_name);
		if(!$query->fetch())
		{
			$query->close();
			unset($_GET['class_id']);
		}
		else
		{
			$query->close();

			// Névsor lekérése
			$students = [];
			$query = $conn->prepare('
				SELECT `students`.`id`, `students`.`omaz`, `students`.`name`, COUNT(`rentals`.`book_code`) AS `rental_db` FROM `students`
				LEFT JOIN `rentals` ON `rentals`.`student_id` = `students`.`id`
				WHERE `students`.`class_id` = ?
				GROUP BY `students`.`id`, `students`.`omaz`, `students`.`name`
				ORDER BY `students`.`name`
			');
			$query->bind_param('i', $_GET['class_id']);
			$query->execute();
			$query->bind_result($id, $omaz, $name, $rental_db);
			while($query->fetch())
			{
				$students[] = array
				(
					'id' => $id,
					'omaz' => $omaz,
					'name' => $name,
					'rental_db' => $rental_db,
				);
			}
			$query->close();
		}
	}
	else
	{
		/*----------  Osztályok listázása - Adatok lekérése  ----------*/

		// Osztályok lekérése
		$classes = [];
		$result = $conn->query('
			SELECT `classes`.`id`, `classes`.`name`, COUNT(DISTINCT `students`.`id`) AS `student_db`, COUNT(`rentals`.`book_code`) AS `rental_db` FROM `classes`
			LEFT JOIN `students` ON `students`.`class_id` = `classes`.`id`
			LEFT JOIN `rentals` ON `rentals`.`student_id` = `students`.`id`
			WHERE `classes`.`school_id` = '.$GLOBALS['SCHOOL_ID'].'
			GROUP BY `classes`.`id`, `classes`.`name`
		');
		while($row = $result->fetch_assoc())
		{
			$classes[] = array
			(
				'id' => $row['id'],
				'name' => $row['name'],
				'student_db' => $row['student_db'],
				'rental_db' => $row['rental_db'],
			);
		}
		$result->close();
	}

	$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Diákok<?php echo !empty($_GET['class_id']) ? ' ('.$class_name.')' : '' ?> - <?php echo $GLOBALS['SCHOOL_NAME'] ?> - TKA</title>
	<?php include 'includes/head.php'; ?>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/myurlap.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/students.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/myurlap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/students.css">
</head>
<body>
	<div id="main-container">
		<?php include 'includes/header.php'; ?>
		<?php if (empty($_GET['class_id'])): ?>
			<!-- Osztályok listázása -->
			<div class="title">
				<h2>Osztályok</h2>
				<?php if ($_SESSION['user']['level'] > 1): ?>
					<button onclick="add_class(this);">Osztály hozzáadása</button>
					<button onclick="import_students(this);">Diákok importálása</button>
				<?php endif ?>
			</div>
			<div class="container">
				<?php foreach ($classes as $class): ?>
					<div class="class">
						<p><?php echo htmlspecialchars($class['name']) ?> (<?php echo $class['student_db'] ?> diák)</p>
						<?php if ($_SESSION['user']['level'] > 1): ?>
							<button onclick="edit_class(this, <?php echo $class['id'] ?>);">Szerkesztés</button>
							<button onclick="delete_class(this, <?php echo $class['id'] ?>);"<?php echo $class['rental_db'] > 0 ? ' disabled title="Az osztály nem törölhető, mert az osztályban vannak vissza nem hozott könyvek"' : '' ?>>Törlés</button>
						<?php endif ?>
						<button onclick="location.href = '?class_id=<?php echo $class['id'] ?>';">Névsor >></button>
					</div>
				<?php endforeach ?>
			</div>
		<?php else: ?>
			<!-- Osztály névsorának listázása -->
			<div class="title">
				<h2><?php echo $class_name ?> osztály névsora</h2>
				<?php if ($_SESSION['user']['level'] > 1): ?>
					<button onclick="add_student(this, <?php echo $_GET['class_id'] ?>);">Diák hozzáadása</button>
				<?php endif ?>
			</div>
			<div class="container">
				<?php foreach ($students as $student): ?>
					<div class="student<?php echo $_SESSION['user']['level'] == 1 ? ' level-diak' : '' ?>">
						<p><?php echo htmlspecialchars($student['omaz']) ?></p>
						<p title="<?php echo htmlspecialchars($student['name']) ?>" class="ellipsis_text"><?php echo htmlspecialchars($student['name']) ?></p>
						<p>kikölcsönzött könyvek: <?php echo $student['rental_db'] ?></p>
						<?php if ($_SESSION['user']['level'] > 1): ?>
							<button onclick="edit_student(this, <?php echo $student['id'] ?>);">Szerkesztés</button>
							<button onclick="delete_student(this, <?php echo $student['id'] ?>);"<?php echo $student['rental_db'] > 0 ? ' disabled title="A diák nem törölhető, mert vannak vissza nem hozott könyvei"' : '' ?>>Törlés</button>
						<?php endif ?>
						<a href="kolcsonzesek?student_id=<?php echo $student['id'] ?>">Kölcsönzések >></a>
					</div>
				<?php endforeach ?>
			</div>
		<?php endif; ?>
		<?php include 'includes/footer.php' ?>
	</div>
</body>
</html>
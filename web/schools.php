<?php
	global $conn;

	// Iskolák lekérése
	$schools = [];
	$result = $conn->query("
		SELECT `schools`.`id`, `schools`.`code`, `schools`.`name`, GROUP_CONCAT(`users`.`name` SEPARATOR ', ') AS `admins`,
			(
				SELECT COUNT(*) FROM `booktypes`
				INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
				WHERE `bookcategories`.`school_id` = `schools`.`id`
			) AS `booktype_db`,
			(
				SELECT COUNT(*) FROM `books`
				INNER JOIN `booktypes` ON `booktypes`.`id` = `books`.`type_id`
				INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
				WHERE `bookcategories`.`school_id` = `schools`.`id`
			) AS `book_db`
		FROM `schools`
		LEFT JOIN `users` ON `users`.`school_id` = `schools`.`id` AND `users`.`level` = 3
		WHERE `schools`.`id` <> 1
		GROUP BY `schools`.`id`
		ORDER BY `schools`.`name`
	");
	while($row = $result->fetch_assoc())
	{
		$schools[] = array
		(
			'id' => $row['id'],
			'code' => $row['code'],
			'name' => $row['name'],
			'admins' => $row['admins'],
			'booktype_db' => $row['booktype_db'],
			'book_db' => $row['book_db'],
		);
	}
	$result->close();

	$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Iskolák - TKA</title>
	<?php include 'includes/head.php'; ?>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/myurlap.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/schools.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/myurlap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/schools.css">
</head>
<body>
	<div id="main-container">
		<?php include 'includes/sysadmin_header.php'; ?>
		<div class="title">
			<h2>Iskolák</h2>
			<button onclick="add_school(this);">Hozzáadás</button>
		</div>
		<div class="container">
			<?php foreach ($schools as $school): ?>
				<div class="school">
					<div class="name">
						<p><?php echo htmlspecialchars($school['name']).' ['.htmlspecialchars($school['code']).']' ?></p>
					</div>
					<div class="details">
						<p class="admins"><span>Adminisztrátor(ok):</span><?php echo htmlspecialchars($school['admins'] ? $school['admins'] : '-') ?></p>
						<p class="books"><span>Könyvek száma:</span><?php echo $school['booktype_db'].' típus, '.$school['book_db'].' példányszám' ?></p>
					</div>
					<div class="buttons">
						<a href="<?php echo WEB_ROOT.'/'.htmlspecialchars(urlencode($school['code'])).'/' ?>">Belépés az iskola felületére >></a>
						<button onclick="edit_school(this, <?php echo $school['id'] ?>);">Szerkesztés</button>
						<button onclick="delete_school(this, <?php echo $school['id'] ?>);">Törlés</button>
					</div>
				</div>
			<?php endforeach ?>
		</div>
		<?php include 'includes/footer.php'; ?>
	</div>
</body>
</html>
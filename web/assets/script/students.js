/*----------  Osztályok kezelése  ----------*/
const urlap_class = new myurlap('class', new Object
({
	content: create_element('div', { id: 'myurlap-class_content' },
	[
		create_element('div', {},
		[
			create_element('p', {}, [ 'Osztály neve: ' ]),
			create_element('input', { type: 'text', attr: { 'maxlength': '255' } }),
		]),
		create_element('div', { style: { 'display': 'none' } },
		[
			create_element('p', {}, [ 'Diákok száma: ' ]),
			create_element('p'),
		]),
	]),
	init: function()
	{
		this.input = new Object
		({
			'name': this.content.firstChild.lastChild,
			'student_db_div': this.content.lastChild,
			'student_db': this.content.lastChild.lastChild,
		});
	},
	show: function(modify, data)
	{
		if(modify)
		{
			this.title = 'Osztály módosítása';
			this.save_text = 'Módosítás';
			this.input['name'].value = data['name'];
			this.input['student_db_div'].style['display'] = '';
			this.input['student_db'].innerHTML = data['student_db'];
		}
		else
		{
			this.title = 'Osztály hozzáadása';
			this.save_text = 'Hozzáadás';
			return this.input['name'];
		}
	},
	validate: function(modify)
	{
		if(this.input['name'].value.trim() == '')
		{
			mymessage.show('Hibás paraméterek', 'Az osztály nevének megadása kötelező', 'Oké', () => this.input['name'].focus());
			return false;
		}
		return true;
	},
	store: function(modify, closing)
	{
		if(!modify && this.input['name'].value.trim() != '' || modify && this.input['name'].value.trim() != this.o_data['name'])
			this.data['name'] = this.input['name'].value.trim();
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			if(modify)
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('id', this.o_data['id']);
				fd.append('name', this.data['name']);
				myfetch('POST', '/backend/students/modify_class.php', 'json', fd).then(response =>
				{
					mymessage.show('Sikeresen módosítva', '', 'Oké', () => new Promise(() => location.reload()));
				});
			}
			else
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('name', this.data['name']);
				myfetch('POST', '/backend/students/add_class.php', 'json', fd).then(response =>
				{
					mymessage.show('Sikeresen hozzáadva', '', 'Oké', () => new Promise(() => location.reload()));
				});
			}
		});
	},
	reset: function()
	{
		this.input['name'].value = '';
		this.input['student_db_div'].style['display'] = 'none';
		this.input['student_db'].innerHTML = '';
	},
}));
function add_class(button)
{
	button.disabled = true;
	urlap_class.show(null, button);
}
function edit_class(button, id)
{
	button.disabled = true;
	let fd = new FormData();
	fd.append('csrf', csrf);
	fd.append('school_id', school_id);
	fd.append('id', id);
	myfetch('POST', '/backend/students/get_class.php', 'json', fd).then(response =>
	{
		urlap_class.show(response.data, button);
	});
}
function delete_class(button, id)
{
	button.disabled = true;
	mymessage.show('Biztosan törli az osztályt?', '', 'Igen', () =>
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('id', id);
			myfetch('POST', '/backend/students/del_class.php', 'json', fd).then(response =>
			{
				location.reload();
			});
		});
	}, 'Mégsem', () => button.disabled = false);
}

/*----------  Diákok kezelése  ----------*/
const urlap_student = new myurlap('student', new Object
({
	class_id: null,
	content: create_element('div', { id: 'myurlap-student_content' },
	[
		create_element('div', {},
		[
			create_element('p', {}, [ 'Osztály: ' ]),
			create_element('select'),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Teljes név: ' ]),
			create_element('input', { type: 'text', attr: { 'maxlength': '255' } }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'OM azonosító: ' ]),
			create_element('input', { type: 'text', attr: { 'minlength': '11', 'maxlength': '11' } }),
		]),
	]),
	init: function()
	{
		this.input = new Object
		({
			'class': this.content.childNodes[0].lastChild,
			'name': this.content.childNodes[1].lastChild,
			'omaz': this.content.childNodes[2].lastChild,
		});
	},
	show: function(modify, data)
	{
		let fd = new FormData();
		fd.append('csrf', csrf);
		fd.append('school_id', school_id);
		myfetch('POST', '/backend/students/get_classes.php', 'json', fd).then(response =>
		{
			response.data.forEach(class_o =>
			{
				this.input['class'].appendChild(create_element('option', { value: class_o.value }, [ class_o.name ]));
			});
			this.input['class'].value = modify ? data['class_id'] : this.properties['class_id'];
		});
		if(modify)
		{
			this.title = 'Diák szerkesztése';
			this.save_text = 'Módosítás';
			this.input['name'].value = data['name'];
			this.input['omaz'].value = data['omaz'];
		}
		else
		{
			this.title = 'Diák hozzáadása';
			this.save_text = 'Hozzáadás';
			return this.input['name'];
		}
	},
	validate: function(modify)
	{
		if(this.input['class'].childNodes.length == 0)
		{
			mymessage.show('Hibás paraméterek', 'Nem adott meg osztályt', 'Oké', () => this.input['class'].focus());
			return false;
		}
		if(this.input['name'].value.trim() == '')
		{
			mymessage.show('Hibás paraméterek', 'A név megadása kötelező', 'Oké', () => this.input['name'].focus());
			return false;
		}
		if(this.input['omaz'].value.trim().length != 11)
		{
			mymessage.show('Hibás paraméterek', 'Az OM azonosítónak 11 karakter hosszúnak kell lennie', 'Oké', () =>
			{
				this.input['omaz'].select();
				this.input['omaz'].focus();
			});
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(!modify && this.input['class'].value != this.properties['class_id'] || modify && this.input['class'].value != this.o_data['class_id'])
			this.data['class_id'] = this.input['class'].value;
		if(!modify && this.input['name'].value.trim() != '' || modify && this.input['name'].value.trim() != this.o_data['name'])
			this.data['name'] = this.input['name'].value.trim();
		if(!modify && this.input['omaz'].value.trim() != '' || modify && this.input['omaz'].value.trim() != this.o_data['omaz'])
			this.data['omaz'] = this.input['omaz'].value.trim();
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			if(modify)
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('id', this.o_data['id']);
				fd.append('changes', JSON.stringify(this.data));
				myfetch('POST', '/backend/students/modify_student.php', 'json', fd).then(response =>
				{
					if(response.valid)
					{
						mymessage.show('Sikeresen módosítva', '', 'Oké', () => new Promise(() => location.reload()));
					}
					else
					{
						mymessage.show('Hiba', 'Ez az OM azonosító már hozzá van rendelve egy diákhoz', 'Oké', () =>
						{
							this.input['omaz'].select();
							this.input['omaz'].focus();
						});
						reject();
					}
				});
			}
			else
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('class_id', this.data['class_id'] ? this.data['class_id'] : this.properties['class_id']);
				fd.append('name', this.data['name']);
				fd.append('omaz', this.data['omaz']);
				myfetch('POST', '/backend/students/add_student.php', 'json', fd).then(response =>
				{
					if(response.valid)
					{
						mymessage.show('Sikeresen hozzáadva', '', 'Oké', () => new Promise(() => location.reload()));
					}
					else
					{
						mymessage.show('Hiba', 'Ez az OM azonosító már hozzá van rendelve egy diákhoz', 'Oké', () =>
						{
							this.input['omaz'].select();
							this.input['omaz'].focus();
						});
						reject();
					}
				});
			}
		});
	},
	reset: function()
	{
		this.input['class'].innerHTML = '';
		this.input['name'].value = '';
		this.input['omaz'].value = '';
	},
}));
function add_student(button, class_id)
{
	button.disabled = true;
	urlap_student.properties['class_id'] = class_id;
	urlap_student.show(null, button);
}
function edit_student(button, id)
{
	button.disabled = true;
	let fd = new FormData();
	fd.append('csrf', csrf);
	fd.append('school_id', school_id);
	fd.append('id', id);
	myfetch('POST', '/backend/students/get_student.php', 'json', fd).then(response =>
	{
		urlap_student.show(response.data, button);
	});
}
function delete_student(button, id)
{
	button.disabled = true;
	mymessage.show('Biztosan törli a diákot?', '', 'Igen', () =>
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('id', id);
			myfetch('POST', '/backend/students/del_student.php', 'json', fd).then(response =>
			{
				location.reload();
			});
		});
	}, 'Mégsem', () => button.disabled = false);
}

/*----------  Diákok importálása  ----------*/
const urlap_import = new myurlap('import', new Object
({
	content: create_element('div', {},
	[
		create_element('div', {},
		[
			create_element('span', {}, [ 'Osztály: ' ]),
			create_element('select', {}, [ create_element('option', { value: 0 }, [ 'Nincs kiválasztva' ]) ]),
		]),
		create_element('input', { type: 'file', attr: { 'accept': 'text/plain,text/tab-separated-values' } }),
	]),
	init: function()
	{
		this.input = new Object
		({
			'class': this.content.childNodes[0].lastChild,
			'file': this.content.childNodes[1],
		});
	},
	show: function(modify, data)
	{
		this.title = 'Diákok importálása';
		this.save_text = 'Importálás';
		let fd = new FormData();
		fd.append('csrf', csrf);
		fd.append('school_id', school_id);
		myfetch('POST', '/backend/students/get_classes.php', 'json', fd).then(response =>
		{
			response.data.forEach(class_o =>
			{
				this.input['class'].appendChild(create_element('option', { value: class_o.value }, [ class_o.name ]));
			});
		});
		return this.input['class'];
	},
	validate: function(modify)
	{
		if(this.input['class'].value == 0)
		{
			mymessage.show('Hibás paraméterek', 'Nem választott osztályt', 'Oké', () => this.input['class'].focus());
			return false;
		}
		if(this.input['file'].files.length == 0)
		{
			mymessage.show('Hibás paraméterek', 'Nem választott fájlt', 'Oké', () => this.input['file'].focus());
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(this.input['class'].value != 0)
			this.data['class_id'] = this.input['class'].value;
		if(this.input['file'].files.length != 0)
			this.data['file'] = this.input['file'].files[0];
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('class_id', this.data['class_id']);
			fd.append('file', this.data['file']);
			myfetch('POST', '/backend/students/import_students.php', 'json', fd).then(response =>
			{
				mymessage.show('Siker!', response.db + ' diák sikeresen importálva', 'Oké', () => new Promise(() => location.reload()));
			});
		});
	},
	reset: function()
	{
		while(this.input['class'].childNodes.length > 1)
			this.input['class'].lastChild.remove();
		this.input['file'].value = '';
	},
}));
function import_students(button)
{
	button.disabled = true;
	urlap_import.show(null, button);
}

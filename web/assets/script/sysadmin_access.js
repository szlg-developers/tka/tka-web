/*----------  Rendszergazda felhasználók kezelése  ----------*/

const urlap_user = new myurlap('user', new Object
({
	content: create_element('div', { id: 'myurlap-user_content' },
	[
		create_element('div', {},
		[
			create_element('p', {}, [ 'Felhasználónév: ' ]),
			create_element('input', { type: 'text', attr: { 'maxlength': '255' } }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Név: ' ]),
			create_element('input', { type: 'text', attr: { 'maxlength': '255' } }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Jelszó: ' ]),
			create_element('input', { type: 'password' }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Jelszó megerősítése: ' ]),
			create_element('input', { type: 'password' }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Jogosultsági szint: ' ]),
			create_element('span', {}, [ 'Rendszergazda' ]),
		]),
	]),
	init: function()
	{
		this.input = new Object
		({
			'username': this.content.childNodes[0].lastChild,
			'name': this.content.childNodes[1].lastChild,
			'password1': this.content.childNodes[2].lastChild,
			'password2': this.content.childNodes[3].lastChild,
		});
	},
	show: function(modify, data)
	{
		if(modify)
		{
			this.title = 'Felhasználó módosítása';
			this.save_text = 'Mentés';
			this.input['username'].value = data['username'];
			this.input['name'].value = data['name'];
			this.input['password1'].placeholder = 'Nem változott';
		}
		else
		{
			this.title = 'Felhasználó hozzáadása';
			this.save_text = 'Hozzáadás';
			this.input['password1'].placeholder = '';
		}
		return this.input['username'];
	},
	validate: function(modify)
	{
		if(this.input['username'].value.trim() == '')
		{
			mymessage.show('Hiányzó paraméterek', 'A felhasználónév megadása kötelező', 'Oké', () => this.input['username'].focus());
			return false;
		}
		if(!modify && this.input['password1'].value == '')
		{
			mymessage.show('Hiányzó paraméterek', 'A jelszó megadása kötelező', 'Oké', () => this.input['password1'].focus());
			return false;
		}
		if(this.input['password1'].value != this.input['password2'].value)
		{
			mymessage.show('Hibás paraméterek', 'A jelszavaknak egyezniük kell', 'Oké', () =>
			{
				this.input['password2'].select();
				this.input['password2'].focus();
			});
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(!modify && this.input['username'].value.trim() != '' || modify && this.input['username'].value.trim() != this.o_data['username'])
			this.data['username'] = this.input['username'].value.trim();
		if(!modify && this.input['name'].value.trim() != '' || modify && this.input['name'].value.trim() != this.o_data['name'])
			this.data['name'] = this.input['name'].value;
		if(!modify && this.input['password1'].value != '' || modify && this.input['password1'].value != '')
			this.data['password'] = this.input['password1'].value;
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			if(modify)
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('id', this.o_data['id']);
				fd.append('changes', JSON.stringify(this.data));
				myfetch('POST', '/backend/sysadmin_access/modify_user.php', 'json', fd).then(response =>
				{
					if(response.valid)
					{
						mymessage.show('Sikeresen mentve', response.relogin ? 'Újbóli bejelentkezés szükséges' : '', 'Oké', () => new Promise(() => location.reload()));
					}
					else
					{
						mymessage.show('Hiba', 'A felhasználónév már használatban van', 'Oké', () =>
						{
							this.input['username'].select();
							this.input['username'].focus();
						});
						reject();
					}
				});
			}
			else
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('username', this.data['username']);
				fd.append('name', this.data['name']);
				fd.append('password', this.data['password']);
				myfetch('POST', '/backend/sysadmin_access/add_user.php', 'json', fd).then(response =>
				{
					if(response.valid)
					{
						mymessage.show('Sikeresen hozzáadva', '', 'Oké', () => new Promise(() => location.reload()));
					}
					else
					{
						mymessage.show('Hiba', 'A felhasználónév már használatban van', 'Oké', () =>
						{
							this.input['username'].select();
							this.input['username'].focus();
						});
						reject();
					}
				});
			}
		});
	},
	reset: function()
	{
		this.input['username'].value = '';
		this.input['name'].value = '';
		this.input['password1'].value = '';
		this.input['password2'].value = '';
	},
}));

function add_user(button)
{
	button.disabled = true;
	urlap_user.show(null, button);
}
function edit_user(button, id)
{
	button.disabled = true;
	let fd = new FormData();
	fd.append('csrf', csrf);
	fd.append('id', id);
	myfetch('POST', '/backend/sysadmin_access/get_user.php', 'json', fd).then(response =>
	{
		urlap_user.show(response.data, button);
	});
}
function delete_user(button, id)
{
	button.disabled = true;
	mymessage.show('Biztosan törli a felhasználót?', '', 'Mégsem', () => button.disabled = false, 'Törlés', () =>
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('id', id);
			myfetch('POST', '/backend/sysadmin_access/del_user.php', 'json', fd).then(response =>
			{
				resolve();
				mymessage.show('Sikeresen törölve', '', 'Oké', () => new Promise(() => location.reload()));
			});
		});
	});
}

function myfetch(method, url, return_type, data)
{
	const return_limit = (def, key) =>
	{
		let val = (myfetch_limits && myfetch_limits[key] ? myfetch_limits[key] : def).toLowerCase().trim();
		let last = val[val.length - 1];
		val = parseInt(val);
		switch(last)
		{
			case 'g': val *= 1024;
			case 'm': val *= 1024;
			case 'k': val *= 1024;
		}
		return val;
	}

	let post_max_size = return_limit('8M', 'post_max_size');
	let upload_max_filesize = return_limit('2M', 'upload_max_filesize');
	let max_file_uploads = return_limit(50, 'max_file_uploads');

	let post_size = 0;
	let file_db = 0;

	return new Promise((resolve, reject) =>
	{
		if(data instanceof FormData)
		{
			let error = false;
			Array.from(data.entries(), e =>
			{
				if(error)
					return;
				if(typeof e[1] === 'string')
				{
					post_size += e[1].length;
				}
				else
				{
					if(e[1].size > upload_max_filesize && upload_max_filesize > 0)
					{
						mymessage.error('Adatátviteli hiba', 'Egy fájl mérete meghaladta a maximumot.', () => reject(new Error('upload_max_filesize_exception')));
						error = true;
						return;
					}
					post_size += e[1].size;
					++file_db;
				}
			});
			if(error)
				return;
		}

		if(post_size > post_max_size && post_max_size > 0)
		{
			mymessage.error('Adatátviteli hiba', 'Az adatcsomag mérete meghaladta a maximumot.', () => reject(new Error('post_max_size_exception')));
			return;
		}
		if(file_db > max_file_uploads)
		{
			mymessage.error('Adatátviteli hiba', 'A feltölteni kívánt fájlok darabszáma meghaladta a maximumot.', () => reject(new Error('max_file_uploads_exception')));
			return;
		}

		if(file_db > 0)
		{
			var upload_status_mid = mymessage.show('Kérem várjon', 'Feltöltés folyamatban...', null, null, null, null, false);
		}

		url = (typeof myfetch_webroot !== 'undefined' && url && url[0] == '/') ? myfetch_webroot + url : url;

		if(method == 'GET')
			fetch(url,
			{
				'credentials': 'same-origin',
			}).then(r => process_answer(r));
		else
			fetch(url,
			{
				'method': method,
				'body': data,
				'credentials': 'same-origin',
			}).then(r => process_answer(r));

		let process_answer = request =>
		{
			if(file_db > 0)
			{
				mymessage.close(upload_status_mid);
			}

			if(return_type == 'text')
			{
				request.text().then(text => resolve(text));
			}
			else if(return_type == 'json')
			{
				request.text().then(json_text =>
				{
					try
					{
						var response = JSON.parse(json_text);
					}
					catch(e)
					{
						mymessage.error('Szerveroldali hiba', '', () => new Promise(() => location.reload(true)));
						console.log(json_text);
						console.error('Serverside failure');
						return;
					}
					if(!response['success'])
					{
						mymessage.error('Hiba', response['message'], () => new Promise(() => location.reload(true)));
						return;
					}
					resolve(response);
				});
			}
			else
			{
				throw new Error('Unknown type \'' + type + '\'');
			}
		}
	});
}

function create_element(type, properties = {}, children = [])
{
	let elem = document.createElement(type);
	Object.keys(properties).forEach(key =>
	{
		if(key == 'classes')
		{
			properties[key].forEach(c => elem.classList.add(c));
		}
		else if(key == 'attr')
		{
			Object.keys(properties[key]).forEach(a => elem.setAttribute(a, properties[key][a]));
		}
		else if(key == 'style')
		{
			Object.keys(properties[key]).forEach(s => elem.style[s] = properties[key][s]);
		}
		else
		{
			elem[key] = properties[key];
		}
	});
	children.forEach(child =>
	{
		if(typeof child === 'object')
		{
			elem.appendChild(child);
		}
		else
		{
			elem.appendChild(document.createTextNode(child));
		}
	});
	return elem;
}

const mymessage = (new Object
({
	show_animation_time: 200,
	closing_animation_time: 200,
	outclick_enabled: true,
	escape_enabled: true,
	primary_button: 2,
	queue: [],
	init: function()
	{
		this.content = create_element('div', { id: 'mymessage', mymessage: this, onclick: function(e) { if(this.mymessage.outclick_enabled && this.mymessage.x_button.style['visibility'] != 'hidden' && e.target == this) this.mymessage.close(this.mymessage.actual_mid, this.mymessage.button2.style['display'] == 'none' ? 1 : 2); } },
		[
			create_element('div', { id: 'mymessage_content' },
			[
				create_element('div', { id: 'mymessage_main' },
				[
					create_element('span', { id: 'mymessage_title' }, [ 'Cím' ]),
					create_element('span', { id: 'mymessage_x', onclick: () => this.close(this.actual_mid, this.button2.style['display'] == 'none' ? 1 : 2) }, [ '×' ]),
					create_element('p', { id: 'mymessage_text' }, [ 'Leírás' ]),
				]),
				create_element('div', { id: 'mymessage_buttons' },
				[
					create_element('button', { id: 'mymessage_button1', onclick: () => this.close(this.actual_mid, 1) }, [ 'Gomb 1' ]),
					create_element('button', { id : 'mymessage_button2', onclick: () => this.close(this.actual_mid, 2) }, [ 'Gomb 2' ]),
				]),
			]),
		]);
		if(this.escape_enabled)
		{
			window.addEventListener('keyup', e => { if(!e.mycancel && e.keyCode == 27 && document.body.contains(this.content) && this.x_button.style['visibility'] != 'hidden') { this.x_button.click(); e.mycancel = true; } });
		}
		this.cim_resz = this.content.firstChild.firstChild.firstChild;
		this.szoveg_resz = this.content.firstChild.firstChild.lastChild;
		this.button1 = this.content.firstChild.lastChild.firstChild;
		this.button2 = this.content.firstChild.lastChild.lastChild;
		this.x_button = this.content.firstChild.firstChild.childNodes[1];
		this.actual_mid = 0;
		this.mid_count = 0;
		this.disabled = false;
		this.button1.addEventListener('keydown', e => { if(e.keyCode == 39 && this.button2.style['display'] != 'none') this.button2.focus(); });
		this.button2.addEventListener('keydown', e => { if(event.keyCode == 37 && this.button1.style['display'] != 'none') this.button1.focus(); });
		return this;
	},
	show: function(message, description, button1, callback1 = null, button2, callback2 = null, x_button = true, mid = 0, button1_disabled = false, button2_disabled = false, disabled = false)
	{
		if(mid == 0) mid = ++this.mid_count;
		if(document.body.contains(this.content))
		{
			this.queue.push
			({
				mid: mid,
				message: message,
				description: description,
				button1: button1,
				button2: button2,
				button1_disabled: button1_disabled,
				button2_disabled: button2_disabled,
				callback1: callback1,
				callback2: callback2,
				x_button: x_button,
				disabled: disabled,
			});
			return mid;
		}
		this.actual_mid = mid;
		this.cim_resz.innerHTML = message;
		this.szoveg_resz.innerHTML = description;
		if(button1 && !button2 && this.primary_button == 2)
		{
			button2 = button1;
			callback2 = callback1;
			button2_disabled = button1_disabled;
			button1 = null;
			callback1 = null;
			button1_disabled = false;
		}
		else if(button2 && !button1 && this.primary_button == 1)
		{
			button1 = button2;
			callback1 = callback2;
			button1_disabled = button2_disabled;
			button2 = null;
			callback2 = null;
			button2_disabled = false;
		}
		if(!button1)
		{
			this.button1.style['display'] = 'none';
		}
		else
		{
			this.button1.style['display'] = '';
			this.button1.innerHTML = button1;
		}
		if(!button2)
		{
			this.button2.style['display'] = 'none';
		}
		else
		{
			this.button2.style['display'] = '';
			this.button2.innerHTML = button2;
		}
		if(x_button)
			this.x_button.style['visibility'] = 'visible';
		else
			this.x_button.style['visibility'] = 'hidden';
		this.callback1 = callback1;
		this.callback2 = callback2;
		this.button1.disabled = button1_disabled;
		this.button2.disabled = button2_disabled;
		this.disabled = disabled;
		document.body.appendChild(this.content);
		this.content.classList.add('show');
		setTimeout(() => this.content.classList.remove('show'), this.show_animation_time);
		if(button1)
			this.button1.focus();
		else if(button2)
			this.button2.focus();
		return mid;
	},
	error: function(message, description, callback)
	{
		let mid = ++this.mid_count;
		if(document.body.contains(this.content))
		{
			this.queue.unshift
			({
				mid: this.actual_mid,
				message: this.cim_resz.innerHTML,
				description: this.szoveg_resz.innerHTML,
				button1: this.button1.style['display'] == 'none' ? null : this.button1.value,
				button2: this.button1.style['display'] == 'none' ? null : this.button2.value,
				button1_disabled: this.button1.disabled,
				button2_disabled: this.button2.disabled,
				callback1: this.callback1,
				callback2: this.callback2,
				x_button: this.x_button.style['visibility'] == 'visible',
				disabled: this.disabled,
			});
			document.body.removeChild(this.content);
			this.show(message, description, 'Oké', callback, null, null, true, mid);
		}
		else
		{
			this.show(message, description, 'Oké', callback, null, null, true, mid);
		}
		return mid;
	},
	close: function(mid = 0, n = 0)
	{
		if(mid == 0 || mid == this.actual_mid)
		{
			if(this.disabled)
				return;
			this.disabled = true;
			if(n == 1 && this.callback1)
			{
				this.button1.disabled = true;
				var result = this.callback1();
			}
			else if(n == 2 && this.callback2)
			{
				this.button2.disabled = true;
				var result = this.callback2();
			}
			const close_message = () =>
			{
				this.content.classList.add('closing');
				setTimeout(() =>
				{
					document.body.removeChild(this.content);
					this.content.classList.remove('closing');
					if(this.queue.length > 0)
					{
						let a = this.queue.shift();
						this.show(a.message, a.description, a.button1, a.callback1, a.button2, a.callback2, a.x_button, a.mid, a.button1_disabled, a.button2_disabled, a.disabled);
					}
				}, this.closing_animation_time);
			};
			if(result instanceof Promise)
				result.then(() => close_message());
			else
				close_message();
		}
		else
		{
			for(let i in this.queue)
			{
				if(this.queue[i].mid == mid)
				{
					if(this.queue[i].disabled)
						return;
					if(n == 1 && this.queue[i].callback1)
						this.queue[i].callback1();
					else if(n == 2 && this.queue[i].callback2)
						this.queue[i].callback2();
					this.queue.splice(i, 1);
					break;
				}
			}
		}
	},
})).init();

class myurlap
{
	constructor(keyword, properties = {})
	{
		this.data = new Object();
		this.original_data = new Object();
		this.original_overflow = '';
		this.show_animation_time = 200;
		this.closing_animation_time = 200;

		this.content = properties['content'] !== undefined ? properties['content'] : '';
		this.outclick_enabled = properties['outclick_enabled'] !== undefined ? properties['outclick_enabled'] : false;
		this.escape_enabled = properties['escape_enabled'] !== undefined ? properties['escape_enabled'] : false;
		this.beforeunload_enabled = properties['beforeunload_enabled'] !== undefined ? properties['beforeunload_enabled'] : true;
		this.empty_array_enabled = properties['empty_array_enabled'] !== undefined ? properties['empty_array_enabled'] : false;
		this.func = [];
		this.properties = [];
		Object.keys(properties).forEach(key =>
		{
			if(this.functions.includes(key))
				this.func[key] = properties[key];
			else if(!this.options.includes(key))
				this.properties[key] = properties[key];
		});

		this.base_div = create_element('div', { id: 'myurlap' + (keyword ? '-' + keyword : ''), classes: [ 'myurlap' ], urlap: this, disabled: false, onclick: function(e) { if(this.urlap.outclick_enabled && e.target == this && !this.disabled) this.urlap.closex(this); } },
		[
			create_element('div', { classes: [ 'myurlap_content' ] },
			[
				create_element('div', { classes: [ 'myurlap_head' ] },
				[
					create_element('span', { classes: [ 'myurlap_title' ] }, [ 'Űrlap címe' ]),
					create_element('span', { classes: [ 'myurlap_x' ], urlap: this, disabled: false, onclick: function() { if(!this.disabled) this.urlap.closex(this); } }, [ '×' ]),
				]),
				this.content,
				create_element('div', { classes: [ 'myurlap_buttons' ] },
				[
					create_element('button', { classes: [ 'myurlap_cancel' ], urlap: this, onclick: function() { this.urlap.closex(this); } }, [ 'Mégse' ]),
					create_element('button', { classes: [ 'myurlap_ok' ], urlap: this, onclick: function() { this.urlap.save(this); } }, [ 'Mentés' ]),
				]),
			]),
		]);

		this.cancel_button = this.base_div.firstChild.lastChild.firstChild;
		this.ok_button = this.base_div.firstChild.lastChild.lastChild;
		this.x_button = this.base_div.firstChild.firstChild.lastChild;

		this.cancel_button.addEventListener('keydown', e => { if(e.keyCode == 39 && this.ok_button.style['display'] != 'none') this.ok_button.focus(); });
		this.ok_button.addEventListener('keydown', e => { if(event.keyCode == 37 && this.cancel_button.style['display'] != 'none') this.cancel_button.focus(); });

		if(this.escape_enabled)
		{
			window.addEventListener('keyup', e => { if(!e.mycancel && e.keyCode == 27 && document.body.contains(this.content) && !document.body.contains(mymessage.content) && this.x_button.style['visibility'] != 'hidden') { this.x_button.click(); e.mycancel = true; } });
		}

		return this.call_func('init');
	}

	get functions()
	{
		return ['init','show','validate','store','save','reset','close'];
	}
	get options()
	{
		return ['content','outclick_enabled','escape_enabled','beforeunload_enabled','empty_array_enabled','base_div'];
	}
	get o_data()
	{
		return this.original_data;
	}

	set title(html)
	{
		this.base_div.firstChild.firstChild.firstChild.innerHTML = html;
	}
	set cancel_text(text)
	{
		this.cancel_button.innerHTML = '';
		this.cancel_button.appendChild(document.createTextNode(text));
	}
	set save_text(text)
	{
		this.ok_button.innerHTML = '';
		this.ok_button.appendChild(document.createTextNode(text));
	}

	// protected void remove_beforeunload
	remove_beforeunload()
	{
		if(this.beforeunload_enabled)
		{
			window.removeEventListener('beforeunload', this.beforeunload_func);
		}
	}

	// public void show
	show(data, button = {})
	{
		button.disabled = true;
		this.original_data = data;

		return this.call_func('show', [ this.original_data != null, data ], (focus_e = null) =>
		{
			this.original_overflow = document.body.style['overflow'];
			document.body.style['overflow'] = 'hidden';
			document.body.appendChild(this.base_div);
			this.base_div.classList.add('show');
			setTimeout(() => this.base_div.classList.remove('show'), this.show_animation_time);
			if(focus_e)
			{
				focus_e.focus();
				if(focus_e.select)
					focus_e.select();
			}
			else
				this.cancel_button.focus();
			this.beforeunload_func = (event) =>
			{
				if(this.beforeunload_enabled && (this.store() instanceof Promise || this.changed()) && event.explicitOriginalTarget == document)
				{
					let message = 'Nem mentett módosításai lehetnek. Biztosan elhagyja az oldalt?';
					event.returnValue = message;
					return message;
				}
			};
			window.addEventListener('beforeunload', this.beforeunload_func);
			button.disabled = false;
		});
	}

	// private boolean validate
	validate()
	{
		return !this.func['validate'] || this.call_func('validate', [ this.original_data != null ]);
	}

	// private void store
	store()
	{
		this.data = new Object();
		return this.call_func('store', [ this.original_data != null ]);
	}

	// private boolean changed
	changed()
	{
		const haschange = o =>
		{
			if(this.empty_array_enabled && Array.isArray(o))
				return true;
			for(let k in o)
				if(typeof o[k] != 'object')
					return true;
				else if(haschange(o[k]))
					return true;
			return false;
		}
		return this.data !== null && (typeof this.data != 'object' || haschange(this.data));
	}

	// public void save
	save(button = {})
	{
		button.disabled = true;
		return this.call_func(this.validate, [], valid =>
		{
			if(valid)
			{
				return this.call_func(this.store, [], () =>
				{
					if(this.changed())
					{
						return this.call_func('save', [ this.original_data != null ], () =>
						{
							return this.call_func(this.close, [ button ]);
						}, () => button.disabled = false);
					}
					else
					{
						return this.call_func(this.close, [ button ]);
					}
				});
			}
			else
			{
				button.disabled = false;
			}
		});
	}

	// private void reset
	reset()
	{
		this.data = new Object();
		return this.call_func('reset');
	}

	// public void closex
	closex(button = {})
	{
		button.disabled = true;
		return new Promise((resolve, reject) =>
		{
			this.call_func(this.store, [], () =>
			{
				if(this.changed())
				{
					let focus_e = document.activeElement;
					mymessage.show('', 'Biztosan elveti a módosításokat?', 'Igen', () =>
					{
						this.call_func(this.close, [ button ], () => resolve());
					}, 'Vissza', () =>
					{
						this.data = new Object();
						button.disabled = false;
						focus_e.focus();
						if(focus_e.select)
							focus_e.select();
					});
				}
				else
				{
					this.call_func(this.close, [ button ], () => resolve());
				}
			});
		});
	}

	// private void close (can be used as protected as well)
	close(button = {})
	{
		return this.call_func('close', [ this.original_data != null ], () =>
		{
			return new Promise((resolve, reject) =>
			{
				this.base_div.classList.add('closing');
				setTimeout(() =>
				{
					window.removeEventListener('beforeunload', this.beforeunload_func);
					document.body.removeChild(this.base_div);
					document.body.style['overflow'] = this.original_overflow;
					this.base_div.classList.remove('closing');
					return this.call_func(this.reset, [], () => { button.disabled = false; resolve(); });
				}, this.closing_animation_time);
			});
		}, () => button.disabled = false);
	}

	// private void call_func
	call_func(key, args = [], callback_resolve = ()=>{}, callback_reject = ()=>{})
	{
		if(typeof key == 'function')
		{
			var func = key;
		}
		else if(this.func[key])
		{
			var func = this.func[key];
		}
		else
		{
			return callback_resolve();
		}
		let result = func.apply(this, args);
		if(result instanceof Promise)
		{
			return new Promise((resolve, reject) =>
			{
				result.then(value =>
				{
					callback_resolve(value);
					resolve(value);
				}).catch(value => callback_reject(value));
			});
		}
		else
		{
			callback_resolve(result);
			return result;
		}
	}
}

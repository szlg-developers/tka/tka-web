/*----------  Felhasználók kezelése  ----------*/
const urlap_user = new myurlap('user', new Object
({
	content: create_element('div', { id: 'myurlap-user_content' },
	[
		create_element('div', {},
		[
			create_element('p', {}, [ 'Felhasználónév: ' ]),
			create_element('input', { type: 'text', attr: { 'maxlength': '255' } }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Név: ' ]),
			create_element('input', { type: 'text', attr: { 'maxlength': '255' } }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Jelszó: ' ]),
			create_element('input', { type: 'password' }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Jelszó megerősítése: ' ]),
			create_element('input', { type: 'password' }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Jogosultsági szint: ' ]),
			create_element('select', {},
			[
				create_element('option', { value: 1 }, [ 'Segítő diák' ]),
				create_element('option', { value: 2 }, [ 'Könyvtáros' ]),
				create_element('option', { value: 3 }, [ 'Adminisztrátor' ]),
			]),
		]),
	]),
	init: function()
	{
		this.input = new Object
		({
			'username': this.content.childNodes[0].lastChild,
			'name': this.content.childNodes[1].lastChild,
			'password1': this.content.childNodes[2].lastChild,
			'password2': this.content.childNodes[3].lastChild,
			'level': this.content.childNodes[4].lastChild,
		});
	},
	show: function(modify, data)
	{
		if(modify)
		{
			this.title = 'Felhasználó módosítása';
			this.save_text = 'Mentés';
			this.input['username'].value = data['username'];
			this.input['name'].value = data['name'];
			this.input['password1'].placeholder = 'Nem változott';
			this.input['level'].value = data['level'];
			if(data.id == data.user_id)
			{
				this.input['level'].disabled = true;
				this.input['level'].style['cursor'] = 'not-allowed';
				this.input['level'].title = 'Saját fiók jogosultsági szintje nem módosítható'
			}
		}
		else
		{
			this.title = 'Felhasználó hozzáadása';
			this.save_text = 'Hozzáadás';
			this.input['password1'].placeholder = '';
			return this.input['username'];
		}
	},
	validate: function(modify)
	{
		if(this.input['username'].value.trim() == '')
		{
			mymessage.show('Hiányzó paraméterek', 'A felhasználónév megadása kötelező', 'Oké', () => this.input['username'].focus());
			return false;
		}
		if(!modify && this.input['password1'].value == '')
		{
			mymessage.show('Hiányzó paraméterek', 'A jelszó megadása kötelező', 'Oké', () => this.input['password1'].focus());
			return false;
		}
		if(this.input['password1'].value != this.input['password2'].value)
		{
			mymessage.show('Hibás paraméterek', 'A jelszavaknak egyezniük kell', 'Oké', () =>
			{
				this.input['password2'].select();
				this.input['password2'].focus();
			});
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(!modify && this.input['username'].value.trim() != '' || modify && this.input['username'].value.trim() != this.o_data['username'])
			this.data['username'] = this.input['username'].value;
		if(!modify && this.input['name'].value.trim() != '' || modify && this.input['name'].value.trim() != this.o_data['name'])
			this.data['name'] = this.input['name'].value;
		if(!modify && this.input['password1'].value != '' || modify && this.input['password1'].value != '')
			this.data['password'] = this.input['password1'].value;
		if(!modify && this.input['level'].value != 1 || modify && this.input['level'].value != this.o_data['level'])
			this.data['level'] = this.input['level'].value;
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			if(modify)
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('id', this.o_data['id']);
				fd.append('changes', JSON.stringify(this.data));
				myfetch('POST', '/backend/access/modify_user.php', 'json', fd).then(response =>
				{
					if(response.valid)
					{
						mymessage.show('Sikeresen mentve', response.relogin ? 'Újbóli bejelentkezés szükséges' : '', 'Oké', () => new Promise(() => location.reload()));
					}
					else
					{
						mymessage.show('Hiba', 'A felhasználónév már használatban van', 'Oké', () =>
						{
							this.input['username'].select();
							this.input['username'].focus();
						});
						reject();
					}
				});
			}
			else
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('username', this.data['username']);
				fd.append('name', this.data['name']);
				fd.append('password', this.data['password']);
				fd.append('level', this.data['level'] ? this.data['level'] : 1);
				myfetch('POST', '/backend/access/add_user.php', 'json', fd).then(response =>
				{
					if(response.valid)
					{
						mymessage.show('Sikeresen hozzáadva', '', 'Oké', () => new Promise(() => location.reload()));
					}
					else
					{
						mymessage.show('Hiba', 'A felhasználónév már használatban van', 'Oké', () =>
						{
							this.input['username'].select();
							this.input['username'].focus();
						});
						reject();
					}
				});
			}
		});
	},
	reset: function()
	{
		this.input['username'].value = '';
		this.input['name'].value = '';
		this.input['password1'].value = '';
		this.input['password2'].value = '';
		this.input['level'].value = 1;
		this.input['level'].disabled = false;
		this.input['level'].style['cursor'] = '';
		this.input['level'].title = '';
	},
}));
function add_user(button)
{
	button.disabled = true;
	urlap_user.show(null, button);
}
function edit_user(button, id)
{
	button.disabled = true;
	let fd = new FormData();
	fd.append('csrf', csrf);
	fd.append('school_id', school_id);
	fd.append('id', id);
	myfetch('POST', '/backend/access/get_user.php', 'json', fd).then(response =>
	{
		urlap_user.show(response.data, button);
	});
}
function delete_user(button, id)
{
	button.disabled = true;
	mymessage.show('Biztosan törli a felhasználót?', '', 'Igen', () =>
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('id', id);
			myfetch('POST', '/backend/access/del_user.php', 'json', fd).then(response =>
			{
				resolve();
				mymessage.show('Sikeresen törölve', '', 'Oké', () => new Promise(() => location.reload()));
			});
		});
	}, 'Mégsem', () => button.disabled = false);
}

/*----------  Alkalmazáskulcsok kezelése  ----------*/
const urlap_appkey = new myurlap('appkey', new Object
({
	data: null,
	content: create_element('div', { id: 'myurlap-appkey_content' },
	[
		create_element('div', {},
		[
			create_element('p', {}, [ 'Kulcs megnevezése: ' ]),
			create_element('input', { type: 'text', required: true, attr: { 'maxlength': '255' } }),
		]),
		create_element('div', { classes: [ 'fingerprint_div' ] },
		[
			create_element('p', {}, [ 'SHA-256 ujjlenyomat: ' ]),
			create_element('p', { classes: [ 'data_text', 'fingerprint' ] }),
		]),
		create_element('div', { style: { 'display': 'none' } },
		[
			create_element('p', {}, [ 'Utoljára használva: ' ]),
			create_element('p', { classes: [ 'data_text' ] }),
		]),
		create_element('div', { classes: [ 'qrcode_div' ], style: { 'display': 'none' } },
		[
			create_element('p', {}, [ 'Szkennelje be a QR kódot, a telefonos alkalmazásban!' ]),
			create_element('div'),
		]),
	]),
	init: function()
	{
		this.input = new Object
		({
			'name': this.content.childNodes[0].lastChild,
			'fingerprint': this.content.childNodes[1].lastChild,
			'lastused_div': this.content.childNodes[2],
			'lastused': this.content.childNodes[2].lastChild,
			'qrcode_div': this.content.childNodes[3],
			'qrcode': this.content.childNodes[3].lastChild,
		});
	},
	show: function(modify, data)
	{
		if(modify)
		{
			this.title = 'Alkalmazáskulcs megtekintése';
			this.save_text = 'Mentés';
			this.input['name'].value = data['name'];
			this.input['fingerprint'].appendChild(document.createTextNode(data['fingerprint']));
			this.input['lastused'].appendChild(document.createTextNode((data['lastused'] ? data['lastused'] : 'Soha') + (data['registered'] ? '' : ' (nem aktivált)')));
			this.input['lastused_div'].style['display'] = '';
			if(!data['registered'])
			{
				this.input['qrcode_div'].style['display'] = '';
				let qrc = new QRCode(this.input['qrcode'], data['qrcode_url']);
				qrc._el.title = '';
			}
		}
		else
		{
			this.title = 'Alkalmazáskulcs hozzáadása';
			this.save_text = 'Mentés';
			this.input['qrcode_div'].style['display'] = '';
			return new Promise((resolve, reject) =>
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				myfetch('POST', '/backend/access/add_appkey.php', 'json', fd).then(response =>
				{
					this.properties['data'] = { 'id': response['id'], 'name': response['name'] };
					this.input['name'].value = response['name'];
					this.input['fingerprint'].appendChild(document.createTextNode(response['fingerprint']));
					let qrc = new QRCode(this.input['qrcode'], response['qrcode_url']);
					qrc._el.title = '';
					resolve(this.input['name']);
				});
			});
		}
	},
	validate: function(modify)
	{
		if(this.input['name'].value.trim() == '')
		{
			mymessage.show('Hibás paraméterek', 'A kulcs neve nem lehet üres', 'Oké', () => this.input['name'].focus());
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(modify && this.input['name'].value.trim() != this.o_data['name'] || !modify)
			this.data['name'] = this.input['name'].value;
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			if(modify)
				fd.append('id', this.o_data['id']);
			else
				fd.append('id', this.properties['data']['id']);
			fd.append('name', this.data['name']);
			myfetch('POST', '/backend/access/modify_appkey.php', 'json', fd).then(response =>
			{
				mymessage.show('Sikeresen mentve', '', 'Oké', () => new Promise(() => location.reload()));
			});
		});
	},
	close: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			if(modify)
			{
				resolve();
			}
			else
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('id', this.properties['data']['id']);
				myfetch('POST', '/backend/access/del_appkey.php', 'json', fd).then(response =>
				{
					resolve();
				});
			}
		});
	},
	reset: function()
	{
		this.input['name'].value = '';
		this.input['fingerprint'].innerHTML = '';
		this.input['lastused'].innerHTML = '';
		this.input['lastused_div'].style['display'] = 'none';
		this.input['qrcode'].innerHTML = '';
		this.input['qrcode_div'].style['display'] = 'none';
		this.properties['data'] = null;
	},
}));
function add_appkey(button)
{
	button.disabled = true;
	urlap_appkey.show(null, button);
}
function view_appkey(button, id)
{
	button.disabled = true;
	let fd = new FormData();
	fd.append('csrf', csrf);
	fd.append('school_id', school_id);
	fd.append('id', id);
	myfetch('POST', '/backend/access/get_appkey.php', 'json', fd).then(response =>
	{
		urlap_appkey.show(response.data, button);
	});
}
function delete_appkey(button, id)
{
	button.disabled = true;
	mymessage.show('Biztosan törli a kulcsot?', '', 'Igen', () =>
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('id', id);
			myfetch('POST', '/backend/access/del_appkey.php', 'json', fd).then(response =>
			{
				location.reload()
			});
		});
	}, 'Mégsem', () => button.disabled = false);
}

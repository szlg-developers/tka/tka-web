/*----------  Kölcsönzés felvétele diákhoz  ----------*/
const urlap_student_rental = new myurlap('student_rental', new Object
({
	student_id: null,
	content: create_element('div', {},
	[
		create_element('div', {},
		[
			create_element('p', {}, [ 'Diák: ' ]),
			create_element('p'),
		]),
		create_element('div', {},
		[
			create_element('div', {},
			[
				create_element('label', {}, [ create_element('input',
				{
					type: 'radio',
					name: 'student_rental_code_input',
					checked: true,
					onclick: function()
					{
						this.parentNode.parentNode.nextSibling.nextSibling.style['display'] = 'none';
						this.parentNode.parentNode.nextSibling.style['display'] = '';
					},
				}), ' Kód' ]),
				create_element('label', {}, [ create_element('input',
				{
					type: 'radio',
					name: 'student_rental_code_input',
					onclick: function()
					{
						this.parentNode.parentNode.nextSibling.style['display'] = 'none';
						this.parentNode.parentNode.nextSibling.nextSibling.style['display'] = '';
					},
				}), ' Kikeresem' ]),
			]),
			create_element('div', {},
			[
				create_element('p', {}, [ 'Könyv kódja: ' ]),
				create_element('input',
				{
					type: 'text',
					attr: { 'maxlength': '32' },
					data_valid: false,
					onchange: function()
					{
						if(this.value.trim() != '')
						{
							this.nextSibling.innerHTML = '...';
							this.nextSibling.style['color'] = '';
							let fd = new FormData();
							fd.append('csrf', csrf);
							fd.append('school_id', school_id);
							fd.append('code', this.value.trim());
							myfetch('POST', '/backend/rentals/get_book.php', 'json', fd).then(response =>
							{
								if(!response.valid)
								{
									this.nextSibling.innerHTML = 'Ezzel a kóddal egy könyv sem rendelkezik';
									this.nextSibling.style['color'] = 'red';
									this.data_valid = false;
								}
								else
								{
									this.nextSibling.innerHTML = '';
									this.nextSibling.appendChild(document.createTextNode(response.title));
									if(response.available == '0')
									{
										this.nextSibling.innerHTML += ' (kikölcsönözve)';
										this.nextSibling.style['color'] = 'blue';
									}
									else
									{
										this.data_valid = true;
									}
								}
							});
						}
						else
						{
							this.nextSibling.innerHTML = '';
							this.nextSibling.style['color'] = '';
						}
					},
				}),
				create_element('p'),
			]),
			create_element('div', { classes: ['select_div'], style: { 'display': 'none' } },
			[
				create_element('select',
				{
					onchange: function()
					{
						if(this.value != 0)
						{
							let fd = new FormData();
							fd.append('csrf', csrf);
							fd.append('school_id', school_id);
							fd.append('category_id', this.value);
							myfetch('POST', '/backend/rentals/get_types.php', 'json', fd).then(response =>
							{
								response.data.forEach(type =>
								{
									this.nextSibling.appendChild(create_element('option', { value: type.id }, [ type.title ]));
								});
							});
						}
						this.nextSibling.value = 0;
						while(this.nextSibling.childNodes.length != 1)
						{
							this.nextSibling.lastChild.remove();
						}
					},
				}, [ create_element('option', { value: 0 }, [ '- Kategória -' ]) ]),
				create_element('select',
				{
					onchange: function()
					{
						if(this.value != 0)
						{
							let fd = new FormData();
							fd.append('csrf', csrf);
							fd.append('school_id', school_id);
							fd.append('type_id', this.value);
							myfetch('POST', '/backend/rentals/get_books.php', 'json', fd).then(response =>
							{
								response.data.forEach(book =>
								{
									this.nextSibling.appendChild(create_element('option', { value: book.code, disabled: book.available == '0' }, [ book.code + (book.available == '0' ? ' (kikölcsönözve)' : '') ]));
								});
							});
						}
						this.nextSibling.value = 0;
						while(this.nextSibling.childNodes.length != 1)
						{
							this.nextSibling.lastChild.remove();
						}
					},
				}, [ create_element('option', { value: 0 }, [ '- Könyv -' ]) ]),
				create_element('select', {}, [ create_element('option', { value: 0 }, [ '- Kód -' ]) ]),
			]),
		]),
	]),
	init: function()
	{
		this.input = new Object
		({
			'student': this.content.childNodes[0].lastChild,
			'radio_code': this.content.childNodes[1].childNodes[0].firstChild.firstChild,
			'radio_select': this.content.childNodes[1].childNodes[0].lastChild.firstChild,
			'code_input': this.content.childNodes[1].childNodes[1].childNodes[1],
			'code_comment': this.content.childNodes[1].childNodes[1].childNodes[2],
			'select': new Object
			({
				'category': this.content.childNodes[1].childNodes[2].childNodes[0],
				'type': this.content.childNodes[1].childNodes[2].childNodes[1],
				'code': this.content.childNodes[1].childNodes[2].childNodes[2],
			}),
		});
	},
	show: function(modify, data)
	{
		this.title = 'Könyv kikölcsönzése';
		this.save_text = 'Hozzáadás';
		let fd = new FormData();
		fd.append('csrf', csrf);
		fd.append('school_id', school_id);
		myfetch('POST', '/backend/rentals/get_categories.php', 'json', fd).then(response =>
		{
			response.data.forEach(category =>
			{
				this.input['select']['category'].appendChild(create_element('option', { value: category.id }, [ category.name ]));
			});
		});
		fd.append('id', this.properties['student_id']);
		myfetch('POST', '/backend/rentals/get_student.php', 'json', fd).then(response =>
		{
			this.input['student'].appendChild(document.createTextNode(response.name + ' (' + response.class + ')'));
		});
		return this.input['code_input'];
	},
	validate: function(modify)
	{
		if(this.input['radio_code'].checked && !this.input['code_input'].data_valid)
		{
			mymessage.show('Hibás paraméterek', 'Nem adott meg érvényes könyvkódot', 'Oké', () =>
			{
				this.input['code_input'].select();
				this.input['code_input'].focus();
			});
			return false;
		}
		if(this.input['radio_select'].checked && this.input['select']['code'].value == 0)
		{
			mymessage.show('Hibás paraméterek', 'Nem választott könyvkódot', 'Oké', () => this.input['select']['code'].focus());
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(this.input['code_input'].value.trim() != '' || this.input['select']['category'].value != 0)
		{
			if(this.input['radio_code'].checked)
				this.data['code'] = this.input['code_input'].value.trim();
			else
				this.data['code'] = this.input['select']['code'].value;
		}
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('book_code', this.data['code']);
			fd.append('student_id', this.properties['student_id']);
			myfetch('POST', '/backend/rentals/add_rental.php', 'json', fd).then(response =>
			{
				mymessage.show('Sikeresen hozzáadva', '', 'Oké', () => new Promise(() => location.reload()));
			});
		});
	},
	reset: function()
	{
		this.input['student'].innerHTML = '';
		this.input['radio_code'].checked = true;
		this.input['radio_select'].checked = false;
		this.input['radio_code'].click();
		this.input['code_input'].value = '';
		this.input['code_comment'].innerHTML = '';
		this.input['code_comment'].style['color'] = '';
		while(this.input['select']['category'].childNodes.length != 1)
			this.input['select']['category'].lastChild.remove();
		this.input['select']['category'].onchange();
		this.input['select']['type'].onchange();
		this.properties['student_id'] = null;
	},
}));
function add_student_rental(button, student_id)
{
	button.disabled = true;
	urlap_student_rental.properties['student_id'] = student_id;
	urlap_student_rental.show(null, button);
}

/*----------  Kölcsönzés felvétele könyvhöz  ----------*/
const urlap_book_rental = new myurlap('book_rental', new Object
({
	book_code: null,
	content: create_element('div', {},
	[
		create_element('div', {},
		[
			create_element('p', {}, [ 'Könyv kódja: ' ]),
			create_element('p'),
			create_element('img', { src: myfetch_webroot + '/assets/img/loading.gif' }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Diák: ' ]),
			create_element('select',
			{
				onchange: function()
				{
					if(this.value != 0)
					{
						let fd = new FormData();
						fd.append('csrf', csrf);
						fd.append('school_id', school_id);
						fd.append('class_id', this.value);
						myfetch('POST', '/backend/rentals/get_students.php', 'json', fd).then(response =>
						{
							response.data.forEach(student =>
							{
								this.nextSibling.appendChild(create_element('option', { value: student.id }, [ student.name ]));
							});
						});
					}
					this.nextSibling.value = 0;
					while(this.nextSibling.childNodes.length != 1)
					{
						this.nextSibling.lastChild.remove();
					}
				},
			}, [ create_element('option', { value: 0 }, [ '- Osztály -' ]) ]),
			create_element('select', {}, [ create_element('option', { value: 0 }, [ '- Diák -' ]) ]),
		]),
	]),
	init: function()
	{
		this.input = new Object
		({
			'code': this.content.childNodes[0].childNodes[1],
			'loading': this.content.childNodes[0].childNodes[2],
			'class': this.content.childNodes[1].childNodes[1],
			'student': this.content.childNodes[1].childNodes[2],
		});
	},
	show: function(modify, data)
	{
		this.title = 'Köny kikölcsönzése';
		this.save_text = 'Hozzáadás';
		let fd = new FormData();
		fd.append('csrf', csrf);
		fd.append('school_id', school_id);
		myfetch('POST', '/backend/rentals/get_classes.php', 'json', fd).then(response =>
		{
			response.data.forEach(class_o =>
			{
				this.input['class'].appendChild(create_element('option', { value: class_o.id }, [ class_o.name ]));
			});
		});
		fd.append('code', this.properties['book_code']);
		myfetch('POST', '/backend/rentals/get_title.php', 'json', fd).then(response =>
		{
			this.input['loading'].style['display'] = 'none';
			this.input['code'].appendChild(document.createTextNode(' (' + response.data + ')'));
		});
		this.input['code'].appendChild(document.createTextNode(this.properties['book_code']));
		return this.input['class'];
	},
	validate: function(modify)
	{
		if(this.input['student'].value == 0)
		{
			mymessage.show('Hibás paraméterek', 'Nem adott meg diákot.', 'Oké', () => this.input['student'].focus());
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(this.input['class'].value != 0)
			this.data['student_id'] = this.input['student'].value;
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('book_code', this.properties['book_code']);
			fd.append('student_id', this.data['student_id']);
			myfetch('POST', '/backend/rentals/add_rental.php', 'json', fd).then(response =>
			{
				mymessage.show('Sikeresen hozzáadva', '', 'Oké', () => new Promise(() => location.reload()));
			});
		});
	},
	reset: function()
	{
		this.input['code'].innerHTML = '';
		this.input['loading'].style['display'] = '';
		this.input['class'].value = 0;
		while(this.input['class'].childNodes.length != 1)
			this.input['class'].lastChild.remove();
		this.input['class'].onchange();
		this.properties['book_code'] = null;
	},
}));
function add_book_rental(button, book_code)
{
	button.disabled = true;
	urlap_book_rental.properties['book_code'] = book_code;
	urlap_book_rental.show(null, button);
}

/*----------  Kölcsönzés törlése (mindkét résznél)  ----------*/
function del_rental(button, book_code)
{
	button.disabled = true;
	mymessage.show('Biztosan törölni szeretné a kölcsönzést?', 'Könyv kódja: ' + book_code, 'Igen', () =>
	{
		let fd = new FormData();
		fd.append('csrf', csrf);
		fd.append('school_id', school_id);
		fd.append('book_code', book_code);
		myfetch('POST', '/backend/rentals/del_rental.php', 'json', fd).then(response =>
		{
			location.reload();
		});
	}, 'Mégsem', () => button.disabled = false);
}

/*----------  Iskolák kezelése  ----------*/

const urlap_school = new myurlap('school', new Object
({
	content: create_element('div', {},
	[
		create_element('div', {},
		[
			create_element('p', {}, [ 'Név: ' ]),
			create_element('input', { type: 'text', classes: ['school_name'], attr: { 'maxlength': '255' } }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Egyedi kód (iskola pár karakteres "monogramja"): ' ]),
			create_element('input', { type: 'text', attr: { 'maxlength': '32' } }),
		]),
		create_element('div', {},
		[
			create_element('a', {}, [ 'Felhasználók kezelése >>' ]),
		]),
	]),
	init: function()
	{
		this.input = new Object
		({
			'name': this.content.childNodes[0].lastChild,
			'code': this.content.childNodes[1].lastChild,
			'users_div': this.content.childNodes[2].lastChild,
		});
	},
	show: function(modify, data)
	{
		if(modify)
		{
			this.title = 'Iskola adatainak módosítása';
			this.save_text = 'Mentés';
			this.input['name'].value = data['name'];
			this.input['code'].value = data['code'];
			this.input['users_div'].href = myfetch_webroot + '/' + data['code'] + '/hozzaferesek';
			this.input['users_div'].style['display'] = '';
		}
		else
		{
			this.title = 'Iskola hozzáadása';
			this.save_text = 'Hozzáadás';
			this.input['users_div'].style['display'] = 'none';
			return this.input['name'];
		}
	},
	validate: function(modify)
	{
		if(this.input['name'].value.trim() == '')
		{
			mymessage.show('Hiányzó paraméterek', 'Az iskola nevének megadása kötelező', 'Oké', () => this.input['name'].focus());
			return false;
		}
		if(this.input['code'].value.trim() == '')
		{
			mymessage.show('Hiányzó paraméterek', 'Az iskola egyedi kódjának megadása kötelező', 'Oké', () => this.input['code'].focus());
			return false;
		}
		if(!/^[a-zA-Z0-9-]+$/.test(this.input['code'].value.trim()))
		{
			mymessage.show('Hibás paraméterek', 'Az iskola egyedi kódja csak ékezetmentes betűket, számokat és kötőjelet tartalmazhat', 'Oké', () =>
			{
				this.input['code'].select();
				this.input['code'].focus();
			});
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(!modify && this.input['name'].value.trim() != '' || modify && this.input['name'].value.trim() != this.o_data['name'])
			this.data['name'] = this.input['name'].value.trim();
		if(!modify && this.input['code'].value.trim() != '' || modify && this.input['code'].value.trim() != this.o_data['code'])
			this.data['code'] = this.input['code'].value.trim();
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			if(modify)
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('id', this.o_data['id']);
				fd.append('changes', JSON.stringify(this.data));
				myfetch('POST', '/backend/schools/modify_school.php', 'json', fd).then(response =>
				{
					if(response.valid)
					{
						mymessage.show('Sikeresen módosítva', '', 'Oké', () => new Promise(() => location.reload()));
					}
					else
					{
						mymessage.show('Hiba', 'Az iskola kódneve már foglalt', 'Oké', () =>
						{
							this.input['code'].select();
							this.input['code'].focus();
						});
						reject();
					}
				});
			}
			else
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('name', this.data['name']);
				fd.append('code', this.data['code']);
				myfetch('POST', '/backend/schools/add_school.php', 'json', fd).then(response =>
				{
					mymessage.show('Sikeresen hozzáadva', '', 'Oké', () => new Promise(() => location.reload()));
				});
			}
		});
	},
	reset: function()
	{
		this.input['name'].value = '';
		this.input['code'].value = '';
		this.input['users_div'].href = '';
		this.input['users_div'].style['display'] = '';
	},
}));

function add_school(button)
{
	button.disabled = true;
	urlap_school.show(null, button);
}
function edit_school(button, id)
{
	button.disabled = true;
	let fd = new FormData();
	fd.append('csrf', csrf);
	fd.append('id', id);
	myfetch('POST', '/backend/schools/get_school.php', 'json', fd).then(response =>
	{
		urlap_school.show(response.data, button);
	});
}
function delete_school(button, id)
{
	button.disabled = true;
	mymessage.show('Biztosan törölni szeretné az iskolát?', 'Az iskola összes adata, illetve felhasználója törlésre kerül.<br>A művelet nem vonható vissza!', 'Mégsem', () => button.disabled = false, 'Törlés', () =>
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('id', id);
			myfetch('POST', '/backend/schools/del_school.php', 'json', fd).then(response =>
			{
				location.reload();
			});
		});
	}, 'Mégsem');
}

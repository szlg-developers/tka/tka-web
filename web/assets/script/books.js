/*----------  Könyvkategóriák kezelése  ----------*/
const urlap_category = new myurlap('category', new Object
({
	content: create_element('div', {},
	[
		create_element('div', {},
		[
			create_element('p', {}, [ 'Kategória neve: ' ]),
			create_element('input', { type: 'text', attr: { 'maxlength': '255' } }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Könyvtípusok száma: ' ]),
			create_element('p', { classes: [ 'data_text' ] }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Könyvek száma: ' ]),
			create_element('p', { classes: [ 'data_text' ] }),
		]),
		create_element('div', {},
		[
			create_element('p', {}, [ 'Kikölcsönzött könyvek száma: ' ]),
			create_element('p', { classes: [ 'data_text' ] }),
		]),
	]),
	init: function()
	{
		this.divs = new Object
		({
			'booktype_db': this.content.childNodes[1],
			'book_db': this.content.childNodes[2],
			'rental_db': this.content.childNodes[3],
		});
		this.input = new Object
		({
			'name': this.content.childNodes[0].lastChild,
			'booktype_db': this.divs['booktype_db'].lastChild,
			'book_db': this.divs['book_db'].lastChild,
			'rental_db': this.divs['rental_db'].lastChild,
		});
	},
	show: function(modify, data)
	{
		if(modify)
		{
			this.title = 'Kategória módosítása';
			this.save_text = 'Módosítás';
			this.input['name'].value = data['name'];
			this.input['booktype_db'].innerHTML = data['booktype_db'];
			this.input['book_db'].innerHTML = data['book_db'];
			this.input['rental_db'].innerHTML = data['rental_db'];
		}
		else
		{
			this.title = 'Kategória hozzáadása';
			this.save_text = 'Hozzáadás';
			this.divs['booktype_db'].style['display'] = 'none';
			this.divs['book_db'].style['display'] = 'none';
			this.divs['rental_db'].style['display'] = 'none';
			return this.input['name'];
		}
	},
	validate: function(modify)
	{
		if(this.input['name'].value.trim() == '')
		{
			mymessage.show('Hibás paraméterek', 'A kategória nevének megadása kötelező', 'Oké', () => this.input['name'].focus());
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(!modify && this.input['name'].value.trim() != '' || modify && this.input['name'].value.trim() != this.o_data['name'])
			this.data['name'] = this.input['name'].value.trim();
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			if(modify)
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('id', this.o_data['id']);
				fd.append('name', this.data['name']);
				myfetch('POST', '/backend/books/modify_category.php', 'json', fd).then(response =>
				{
					mymessage.show('Sikeresen módosítva', '', 'Oké', () => new Promise(() => location.reload()));
				});
			}
			else
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('name', this.data['name']);
				myfetch('POST', '/backend/books/add_category.php', 'json', fd).then(response =>
				{
					mymessage.show('Sikeresen hozzáadva', '', 'Oké', () => new Promise(() => location.reload()));
				});
			}
		});
	},
	reset: function()
	{
		this.divs['booktype_db'].style['display'] = '';
		this.divs['book_db'].style['display'] = '';
		this.divs['rental_db'].style['display'] = '';
		this.input['name'].value = '';
		this.input['booktype_db'].innerHTML = '';
		this.input['book_db'].innerHTML = '';
		this.input['rental_db'].innerHTML = '';
	},
}));
function add_category(button)
{
	button.disabled = true;
	urlap_category.show(null, button);
}
function edit_category(button, id)
{
	button.disabled = true;
	let fd = new FormData();
	fd.append('csrf', csrf);
	fd.append('school_id', school_id);
	fd.append('id', id);
	myfetch('POST', '/backend/books/get_category.php', 'json', fd).then(response =>
	{
		urlap_category.show(response.data, button);
	});
}
function del_category(button, id)
{
	button.disabled = true;
	mymessage.show('Biztosan törölni szeretné a kategóriát?', 'Az összes ehhez tartozó könyv és kölcsönzés visszavonhatatlanul törlődni fog', 'Törlés', () =>
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('id', id);
			myfetch('POST', '/backend/books/del_category.php', 'json', fd).then(response =>
			{
				location.reload();
			});
		});
	}, 'Mégsem', () => button.disabled = false);
}

/*----------  Könyvtípusok betöltése  ----------*/
function show_types(button, category_id)
{
	if(button.disabled) return;
	button.disabled = true;

	const showclose = () =>
	{
		button.nextElementSibling.style['display'] = button.nextElementSibling.style['display'] == 'none' ? '' : 'none';
		button.title = button.nextElementSibling.style['display'] == '' ? button.attributes['data-title-close'].value : button.attributes['data-title-open'].value;
	};

	if(button.is_loaded)
	{
		showclose();
		button.disabled = false;
	}
	else
	{
		button.nextElementSibling.nextElementSibling.style['display'] = '';
		button.title = button.attributes['data-title-loading'].value;
		let fd = new FormData();
		fd.append('csrf', csrf);
		fd.append('school_id', school_id);
		fd.append('category_id', category_id);
		myfetch('POST', '/backend/books/get_types.php', 'json', fd).then(response =>
		{
			response.level = parseInt(response.level);
			response.data.forEach(type =>
			{
				button.nextElementSibling.appendChild(create_element('div', { classes: [ 'type' ] },
				[
					create_element('p', { classes: [ 'ellipsis_text' ], title: type.title }, [ type.title ]),
					create_element('span', {}, [ '(' + type.rental_db + '/' + type.book_db + ' kikölcsönözve)' ]),
					create_element('div', {},
					[
						!(response.level > 1) ? '' : create_element('button', { data_type_id: type.id, onclick: function() { edit_type(this, this.data_type_id); } }, [ 'Szerkesztés' ]),
						!(response.level > 1) ? '' : create_element('button', { data_type_id: type.id, onclick: function() { del_type(this, this.data_type_id); } }, [ 'Törlés' ]),
						create_element('a', { href: 'kolcsonzesek?booktype_id=' + type.id }, [ 'Kölcsönzések >>' ]),
					]),
				]));
				if(response.level == 1)
				{
					button.nextElementSibling.lastChild.classList.add('level-diak');
				}
			});
			button.is_loaded = true;
			button.nextElementSibling.nextElementSibling.style['display'] = 'none';
			showclose();
			button.disabled = false;
		});
	}
}

/*----------  Könyvtípusok kezelése  ----------*/
const urlap_type = new myurlap('type', new Object
({
	content: create_element('div', {},
	[
		create_element('div', {},
		[
			create_element('span', {}, [ 'Kategória: ' ]),
			create_element('select'),
		]),
		create_element('div', {},
		[
			create_element('span', {}, [ 'Könyv címe: ' ]),
			create_element('input', { type: 'text', attr: { 'maxlength': '255' } }),
		]),
		create_element('p', {}, [ 'Könyvkódok:' ]),
		create_element('div', { classes: ['codes_div'] },
		[
			create_element('table', {},
			[
				create_element('thead', {},
				[
					create_element('th', {}, [ 'Sorszám' ]),
					create_element('th', {}, [ 'Vonalkód' ]),
					create_element('th', {}, [ 'Kikölcsönözve' ]),
					create_element('th', {}, [ 'Művelet' ]),
				]),
				create_element('tbody'),
			]),
		]),
		create_element('div', {},
		[
			create_element('button', {}, [ 'Hozzáadás' ]),
		]),
	]),
	init: function()
	{
		this.input = new Object
		({
			'category': this.content.childNodes[0].lastChild,
			'title': this.content.childNodes[1].lastChild,
			'codes': this.content.childNodes[3].lastChild.lastChild,
			'addcode': this.content.childNodes[4].lastChild,
		});
		this.deleted_codes = [];
		this.input['addcode'].addEventListener('click', () =>
		{
			this.input['codes'].appendChild(create_element('tr', {},
			[
				create_element('td', {}, [ create_element('span', {}, [ (this.input['codes'].childNodes.length + 1) + '.' ]) ]),
				create_element('td', {}, [ create_element('input', { type: 'text', attr: { 'maxlength': '32' } }) ]),
				create_element('td', {}, [ create_element('span', {}, [ '-' ]) ]),
				create_element('td', {},
				[
					create_element('button',
					{
						onclick: function()
						{
							let node = this.parentNode.parentNode.nextSibling;
							while(node !== null)
							{
								node.firstChild.firstChild.innerHTML = (parseInt(node.firstChild.firstChild.innerHTML) - 1) + '.';
								node = node.nextSibling;
							}
							this.parentNode.parentNode.remove();
						},
					}, [ 'Törlés' ]),
				]),
			]));
			this.input['codes'].lastChild.childNodes[1].firstChild.focus();
		});
	},
	show: function(modify, data)
	{
		let fd = new FormData();
		fd.append('csrf', csrf);
		fd.append('school_id', school_id);
		myfetch('POST', '/backend/books/get_categories.php', 'json', fd).then(response =>
		{
			response.data.forEach(category =>
			{
				this.input['category'].appendChild(create_element('option', { value: category.value }, [ category.name ]));
			});
			if(modify)
			{
				this.input['category'].value = data['category_id'];
			}
		});
		if(modify)
		{
			this.title = 'Könyvtípus módosítása';
			this.save_text = 'Módosítás';
			this.input['title'].value = data['title'];
			data['codes'].forEach(code =>
			{
				this.input['codes'].appendChild(create_element('tr', {},
				[
					create_element('td', {}, [ create_element('span', {}, [ (this.input['codes'].childNodes.length + 1) + '.' ]) ]),
					create_element('td', {}, [ create_element('span', {}, [ code ]) ]),
					create_element('td', {}, [ create_element('span', {}, [ code in data['rentals'] ? data['rentals'][code].name + ' (' + data['rentals'][code].class + ')' : '-' ]) ]),
					create_element('td', {},
					[
						create_element('button',
						{
							bp: this,
							onclick: function()
							{
								mymessage.show('Biztosan törölni szeretné a könyvkódot?', 'A módosítás csak az ablak mentésekor érvényesül', 'Törlés', () =>
								{
									this.bp.deleted_codes.push(this.parentNode.previousSibling.lastChild.innerHTML);
									let node = this.parentNode.parentNode.nextSibling;
									while(node !== null)
									{
										node.firstChild.firstChild.innerHTML = (parseInt(node.firstChild.firstChild.innerHTML) - 1) + '.';
										node = node.nextSibling;
									}
									this.parentNode.parentNode.remove();
								}, 'Mégsem');
							},
						}, [ 'Törlés' ]),
					]),
				]));
			});
		}
		else
		{
			this.title = 'Könyvtípus hozzáadása';
			this.save_text = 'Hozzáadás';
		}
		return this.input['category'];
	},
	validate: function(modify)
	{
		if(this.input['category'].options.length == 0)
		{
			mymessage.show('Hibás paraméterek', 'Nem adott meg kategóriát', 'Oké', () => this.input['category'].focus());
			return false;
		}
		if(this.input['title'].value.trim() == '')
		{
			mymessage.show('Hibás paraméterek', 'A könyv címének megadása kötelező', 'Oké', () => this.input['title'].focus());
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(!modify && this.input['category'].options.length != 0 && this.input['category'].selectedIndex != 0 || modify && this.input['category'].value != this.o_data['category_id'])
			this.data['category_id'] = this.input['category'].value;
		if(!modify && this.input['title'].value.trim() != '' || modify && this.input['title'].value.trim() != this.o_data['title'])
			this.data['title'] = this.input['title'].value.trim();
		this.data['codes'] = { deleted: this.deleted_codes, new: [] };
		for(let i = this.input['codes'].childNodes.length - 1; i >= 0 && this.input['codes'].childNodes[i].childNodes[1].lastChild.nodeName == 'INPUT'; --i)
		{
			if(this.input['codes'].childNodes[i].childNodes[1].lastChild.value != '')
			{
				this.data['codes'].new.push(this.input['codes'].childNodes[i].childNodes[1].lastChild.value);
			}
		}
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			if(modify)
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('id', this.o_data['id']);
				fd.append('changes', JSON.stringify(this.data));
				myfetch('POST', '/backend/books/modify_type.php', 'json', fd).then(response =>
				{
					mymessage.show('Sikeresen módosítva', '', 'Oké', () => new Promise(() => location.reload()));
				});
			}
			else
			{
				let fd = new FormData();
				fd.append('csrf', csrf);
				fd.append('school_id', school_id);
				fd.append('category_id', this.data['category_id'] ? this.data['category_id'] : this.input['category'].options[0].value);
				fd.append('title', this.data['title']);
				fd.append('codes', JSON.stringify(this.data['codes'].new));
				myfetch('POST', '/backend/books/add_type.php', 'json', fd).then(response =>
				{
					mymessage.show('Sikeresen hozzáadva', '', 'Oké', () => new Promise(() => location.reload()));
				});
			}
		});
	},
	reset: function()
	{
		this.input['category'].innerHTML = '';
		this.input['title'].value = '';
		this.input['codes'].innerHTML = '';
	},
}));
function add_type(button)
{
	button.disabled = true;
	urlap_type.show(null, button);
}
function edit_type(button, id)
{
	button.disabled = true;
	let fd = new FormData();
	fd.append('csrf', csrf);
	fd.append('school_id', school_id);
	fd.append('id', id);
	myfetch('POST', '/backend/books/get_type.php', 'json', fd).then(response =>
	{
		urlap_type.show(response.data, button);
	});
}
function del_type(button, id)
{
	button.disabled = true;
	mymessage.show('Biztosan törölni szeretné a könyvtípust?', 'A könyvtípushoz tartozó összes könyv és kölcsönzés törlődni fog', 'Törlés', () =>
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('id', id);
			myfetch('POST', '/backend/books/del_type.php', 'json', fd).then(response =>
			{
				location.reload();
			});
		});
	}, 'Mégsem', () => button.disabled = false);
}

/*----------  Könyvek importálása  ----------*/
const urlap_import = new myurlap('import', new Object
({
	content: create_element('div', {},
	[
		create_element('div', {},
		[
			create_element('span', {}, [ 'Kategória: ' ]),
			create_element('select', {}, [ create_element('option', { value: 0 }, [ 'Nincs kiválasztva' ]) ]),
		]),
		create_element('input', { type: 'file', attr: { 'accept': 'text/plain,text/tab-separated-values' } }),
	]),
	init: function()
	{
		this.input = new Object
		({
			'category': this.content.childNodes[0].lastChild,
			'file': this.content.childNodes[1],
		});
	},
	show: function(modify, data)
	{
		this.title = 'Könyvek importálása';
		this.save_text = 'Importálás';
		let fd = new FormData();
		fd.append('csrf', csrf);
		fd.append('school_id', school_id);
		myfetch('POST', '/backend/books/get_categories.php', 'json', fd).then(response =>
		{
			response.data.forEach(category =>
			{
				this.input['category'].appendChild(create_element('option', { value: category.value }, [ category.name ]));
			});
		});
		return this.input['category'];
	},
	validate: function(modify)
	{
		if(this.input['category'].value == 0)
		{
			mymessage.show('Hibás paraméterek', 'Nem választott kategóriát', 'Oké', () => this.input['category'].focus());
			return false;
		}
		if(this.input['file'].files.length == 0)
		{
			mymessage.show('Hibás paraméterek', 'Nem választott fájlt', 'Oké', () => this.input['file'].focus());
			return false;
		}
		return true;
	},
	store: function(modify)
	{
		if(this.input['category'].value != 0)
			this.data['category_id'] = this.input['category'].value;
		if(this.input['file'].files.length > 0)
			this.data['file'] = this.input['file'].files[0];
	},
	save: function(modify)
	{
		return new Promise((resolve, reject) =>
		{
			let fd = new FormData();
			fd.append('csrf', csrf);
			fd.append('school_id', school_id);
			fd.append('category_id', this.data['category_id']);
			fd.append('file', this.data['file']);
			myfetch('POST', '/backend/books/import_books.php', 'json', fd).then(response =>
			{
				mymessage.show('Siker!', response.db + ' könyv sikeresen importálva', 'Oké', () => new Promise(() => location.reload()));
			});
		});
	},
	reset: function()
	{
		while(this.input['category'].childNodes.length > 1)
			this.input['category'].lastChild.remove();
		this.input['file'].value = '';
	},
}));
function import_books(button)
{
	button.disabled = true;
	urlap_import.show(null, button);
}

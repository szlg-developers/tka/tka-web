<?php
	global $conn;

	// Felhasználók lekérése
	$users = [];
	$result = $conn->query('SELECT `id`, `username`, `name`, `level` FROM `users` WHERE `school_id` = '.$GLOBALS['SCHOOL_ID']);
	while($row = $result->fetch_assoc())
	{
		$users[] = array
		(
			'id' => $row['id'],
			'username' => $row['username'],
			'name' => $row['name'],
			'level' => $row['level'],
		);
	}
	$result->close();

	// Alkalmazáskulcsok lekérése
	$appkeys = [];
	$result = $conn->query('SELECT `id`, `name`, `lastused`, `registered` FROM `appkeys` WHERE `school_id` = '.$GLOBALS['SCHOOL_ID']);
	while($row = $result->fetch_assoc())
	{
		$appkeys[] = array
		(
			'id' => $row['id'],
			'name' => $row['name'],
			'lastused' => $row['lastused'],
			'registered' => $row['registered'],
		);
	}
	$result->close();

	$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Hozzáférések - <?php echo $GLOBALS['SCHOOL_NAME'] ?> - TKA</title>
	<?php include 'includes/head.php'; ?>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/myurlap.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/access.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/qrcodejs/1.0.0/qrcode.min.js" integrity="sha512-CNgIRecGo7nphbeZ04Sc13ka07paqdeTu0WR1IM4kNcpmBAUSHSQX0FslNhTDadL4O5SAGapGt4FodqL8My0mA==" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/myurlap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/access.css">
</head>
<body>
	<div id="main-container">
		<?php include 'includes/header.php'; ?>
		<div class="title">
			<h2>Felhasználók</h2>
			<button onclick="add_user(this);">Hozzáadás</button>
		</div>
		<div class="container">
			<?php foreach ($users as $user): ?>
				<div class="user">
					<div>
						<p><?php echo htmlspecialchars($user['username']).' - '.htmlspecialchars($user['name']).' ('.($user['level'] == 3 ? 'adminisztrátor' : ($user['level'] == 2 ? 'könyvtáros' : 'segítő diák')).')' ?></p>
					</div>
					<div class="buttons">
						<button onclick="edit_user(this, <?php echo $user['id'] ?>);">Szerkesztés</button>
						<button onclick="delete_user(this, <?php echo $user['id'] ?>);"<?php echo $user['id'] == $_SESSION['user']['id'] ? ' disabled title="Saját fiókot nem lehet törölni"' : '' ?>><?php echo $user['id'] == $_SESSION['user']['id'] ? 'Jelenlegi' : 'Törlés' ?></button>
					</div>
				</div>
			<?php endforeach ?>
		</div>
		<div class="title">
			<h2>Alkalmazáskulcsok</h2>
			<button onclick="add_appkey(this);">Hozzáadás</button>
		</div>
		<div class="container">
			<?php foreach ($appkeys as $appkey): ?>
				<div class="appkey">
					<div>
						<p><?php echo htmlspecialchars($appkey['name']) ?></p>
						<p>Utoljára használva: <?php echo ($appkey['lastused'] ? $appkey['lastused'] : 'Soha').($appkey['registered'] ? '' : ' (nem aktivált)') ?></p>
					</div>
					<div class="buttons">
						<button onclick="view_appkey(this, <?php echo $appkey['id'] ?>);">Megtekintés</button>
						<button onclick="delete_appkey(this, <?php echo $appkey['id'] ?>);">Törlés</button>
					</div>
				</div>
			<?php endforeach ?>
		</div>
		<?php include 'includes/footer.php' ?>
	</div>
</body>
</html>
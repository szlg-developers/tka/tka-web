<?php
	global $conn;

	// Iskolák lekérése
	$schools = [];
	$result = $conn->query('SELECT `id`, `name`, `code` FROM `schools` WHERE `id` <> 1');
	while($row = $result->fetch_assoc())
	{
		$schools[] = array
		(
			'id' => $row['id'],
			'name' => $row['name'],
			'code' => $row['code'],
		);
	}
	$result->close();

	$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Bejelentkezés - TKA</title>
	<?php include 'includes/head.php'; ?>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/login.css">
</head>
<body>
	<div id="main-container">
		<h1><?php echo htmlspecialchars(INSTANCE_NAME) ?></h1>
		<h2>Bejelentkezés</h2>
		<form method="POST" action="<?php echo WEB_ROOT ?>/backend/login.php?continue=<?php echo isset($_GET['continue']) ? htmlspecialchars(urlencode($_GET['continue'])) : '' ?>">
			<select name="school">
				<option value="1">-- Nincs iskola kiválasztva --</option>
				<?php foreach ($schools as $school): ?>
					<option value="<?php echo $school['id'] ?>"<?php echo !empty($_COOKIE['lastused_school']) && $_COOKIE['lastused_school'] == $school['id'] ? ' selected' : '' ?>><?php echo htmlspecialchars($school['name']).' ['.htmlspecialchars($school['code']).']' ?></option>
				<?php endforeach ?>
			</select>
			<input type="text" name="username" placeholder="Felhasználónév" autofocus>
			<input type="password" name="password" placeholder="Jelszó">
			<input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf'] ?>">
			<p id="login-error_message"><?php if(isset($_SESSION['login_error'])) { echo $_SESSION['login_error']; unset($_SESSION['login_error']); } ?></p>
			<input type="submit" value="Bejelentkezés">
		</form>
		<?php include 'includes/footer.php' ?>
	</div>
</body>
</html>
<?php
	global $conn;

	// Rendszergazda felhasználók lekérése
	$users = [];
	$result = $conn->query('SELECT `id`, `username`, `name`, `level` FROM `users` WHERE `level` = 4');
	while($row = $result->fetch_assoc())
	{
		$users[] = array
		(
			'id' => $row['id'],
			'username' => $row['username'],
			'name' => $row['name'],
			'level' => $row['level'],
		);
	}
	$result->close();

	$conn->close();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Hozzáférések - TKA</title>
	<?php include 'includes/head.php'; ?>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/myurlap.js"></script>
	<script type="text/javascript" src="<?php echo WEB_ROOT ?>/assets/script/sysadmin_access.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/myurlap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo WEB_ROOT ?>/assets/css/sysadmin_access.css">
</head>
<body>
	<div id="main-container">
		<?php include 'includes/sysadmin_header.php'; ?>
		<div class="title">
			<h2>Rendszergazdák</h2>
			<button onclick="add_user(this);">Hozzáadás</button>
		</div>
		<div class="container">
			<?php foreach ($users as $user): ?>
				<div class="user">
					<div>
						<p><?php echo htmlspecialchars($user['username']).' - '.htmlspecialchars($user['name']).' (rendszergazda)' ?></p>
					</div>
					<div class="buttons">
						<button onclick="edit_user(this, <?php echo $user['id'] ?>);">Szerkesztés</button>
						<button onclick="delete_user(this, <?php echo $user['id'] ?>);"<?php echo $user['id'] == $_SESSION['user']['id'] ? ' disabled title="Saját fiókot nem lehet törölni"' : '' ?>><?php echo $user['id'] == $_SESSION['user']['id'] ? 'Jelenlegi' : 'Törlés' ?></button>
					</div>
				</div>
			<?php endforeach ?>
		</div>
		<?php include 'includes/footer.php'; ?>
	</div>
</body>
</html>
<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(4);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'id');
check_param($_POST['id'], 'integer');

// Iskola törlése
$conn->query('DELETE FROM `schools` WHERE `id` <> 1 AND `id` = '.$_POST['id']);

echo json_encode(array
(
	'success' => true,
));

$conn->close();

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(4);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'id');
check_param($_POST['id'], 'integer');

// Iskola adatainak lekérése
$result = $conn->query('SELECT `name`, `code` FROM `schools` WHERE `id` <> 1 AND `id` = '.$_POST['id']);
if(!($row = $result->fetch_assoc()))
{
	$result->close();
	$conn->close();
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Az iskola nem található',
	)));
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => array
	(
		'id' => $_POST['id'],
		'name' => $row['name'],
		'code' => $row['code'],
	),
));

$conn->close();

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(4);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'id', 'changes');
check_param($_POST['id'], 'integer');
check_param($_POST['changes'], 'json');
check_param($_POST['changes'], 'array', null, 1);
foreach($_POST['changes'] as $key => $value)
{
	switch($key)
	{
		case 'name':
			check_param($value, 'string', 255, 1, true);
			continue 2;
		case 'code':
			check_param($value, 'string', 32, 1, true);
			if(!preg_match('/^[a-zA-Z0-9-]+$/', $value))
				wrong_param();
			continue 2;
		default:
			wrong_param();
	}
}

// Módosító lekérdezés előkészítése (csak azokat frissítjük, amik módosultak)
$query_sets = '';
$query_types = '';
$query_params = [];

if(isset($_POST['changes']['name']))
{
	$query_sets .= '`name` = ?, ';
	$query_types .= 's';
	$query_params[] = $_POST['changes']['name'];
}
if(isset($_POST['changes']['code']))
{
	$query_sets .= '`code` = ?, ';
	$query_types .= 's';
	$query_params[] = $_POST['changes']['code'];
}

// Hiba, ha semmilyen változtatás sem lett elküldve
if($query_sets == '')
{
	missing_param();
}

// Lekérdezés paramétereinek véglegesítése
$func_params = [ $query_types ];
$count = count($query_params);
for($i = 0; $i < $count; ++$i)
	$func_params[] = &$query_params[$i];

// Iskola módosítása (feltéve, hogy egyedi a "kódneve")
$query = $conn->prepare('UPDATE `schools` SET '.substr($query_sets, 0, strlen($query_sets) - 2).' WHERE `id` <> 1 AND `id` = '.$_POST['id']);
call_user_func_array(array($query, 'bind_param'), $func_params);
$query->execute();

echo json_encode(array
(
	'success' => true,
	'valid' => $conn->affected_rows == 1,
));

$query->close();

$conn->close();

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(4);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'name', 'code');
check_param($_POST['name'], 'string', 255, 1, true);
check_param($_POST['code'], 'string', 32, 1, true);
if(!preg_match('/^[a-zA-Z0-9-]+$/', $_POST['code']))
	wrong_param();

// Foglalt iskola "kódnevek" szűrése
if(in_array($_POST['code'], ['bejelentkezes', 'rendszergazda']))
	exit(json_encode(array
	(
		'success' => true,
		'valid' => false,
	)));

// Iskola hozzáadása
$query = $conn->prepare('INSERT INTO `schools` (`name`, `code`, `date`) VALUES (?, ?, NOW())');
$query->bind_param('ss', $_POST['name'], $_POST['code']);
$query->execute();
$query->close();

echo json_encode(array
(
	'success' => true,
	'valid' => $conn->insert_id != 0,
));

$conn->close();

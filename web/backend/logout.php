<?php

session_start();

// CSRF token meglétének és hitelességének ellenőrzése
if(empty($_POST['csrf']) || empty($_SESSION['csrf']) || $_POST['csrf'] != $_SESSION['csrf'])
{
	header('Location: ../bejelentkezes');
	exit;
}

// Felhasználó kijelentkeztetése
unset($_SESSION['user']);

// Átirányítás
header('Location: ../bejelentkezes');

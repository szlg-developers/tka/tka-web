<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(1, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'class_id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['class_id'], 'integer');

// Egy adott osztály névsorának lekérése
$students = [];

$result = $conn->query('
	SELECT `students`.`id`, `students`.`name` FROM `students`
	INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	WHERE `classes`.`school_id` = '.$_POST['school_id'].' AND `classes`.`id` = '.$_POST['class_id'].'
');
while($row = $result->fetch_assoc())
{
	$students[] = array
	(
		'id' => $row['id'],
		'name' => $row['name'],
	);
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => $students,
));

$conn->close();

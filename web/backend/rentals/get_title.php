<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(1, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'code');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['code'], 'string', 32, 1);

// Egy adott könyvkódhoz tartozó könyv címének lekérése
$query = $conn->prepare('
	SELECT `booktypes`.`title` FROM `booktypes`
	INNER JOIN `books` ON `books`.`type_id` = `booktypes`.`id`
	INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
	WHERE `bookcategories`.`school_id` = ? AND `books`.`code` = ?
');
$query->bind_param('is', $_POST['school_id'], $_POST['code']);
$query->execute();
$query->bind_result($title);
if(!$query->fetch())
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A könyvet törölték az adatbázisból',
	)));
}
$query->close();

echo json_encode(array
(
	'success' => true,
	'data' => $title,
));

$conn->close();

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(1, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'code');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['code'], 'string', 32, 1);

// Egy adott könyv adatainak lekérése
$query = $conn->prepare('
	SELECT `booktypes`.`title`, `rentals`.`student_id` IS NULL AS `available` FROM `booktypes`
	INNER JOIN `books` ON `books`.`type_id` = `booktypes`.`id`
	INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
	LEFT JOIN `rentals` ON `rentals`.`book_code` = `books`.`code`
	WHERE `bookcategories`.`school_id` = ? AND `books`.`code` = ?
');
$query->bind_param('is', $_POST['school_id'], $_POST['code']);
$query->execute();
$query->bind_result($title, $available);
if(!$query->fetch())
{
	exit(json_encode(array
	(
		'success' => true,
		'valid' => false,
	)));
}
$query->close();

echo json_encode(array
(
	'success' => true,
	'valid' => true,
	'available' => $available,
	'title' => $title,
));

$conn->close();

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(1, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'category_id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['category_id'], 'integer');

// Egy adott kategóriához tartozó könyvtípusok lekérése
$types = [];

$result = $conn->query('
	SELECT `booktypes`.`id`, `booktypes`.`title` FROM `booktypes`
	INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
	WHERE `bookcategories`.`school_id` = '.$_POST['school_id'].' AND `bookcategories`.`id` = '.$_POST['category_id'].'
	ORDER BY `booktypes`.`title`
');
while($row = $result->fetch_assoc())
{
	$types[] = array
	(
		'id' => $row['id'],
		'title' => $row['title'],
	);
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => $types,
));

$conn->close();

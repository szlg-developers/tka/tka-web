<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(1, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Egy adott diák adatainak lekérése
$result = $conn->query('
	SELECT `students`.`name`, `classes`.`name` AS `class` FROM `students`
	INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	WHERE `classes`.`school_id` = '.$_POST['school_id'].' AND `students`.`id` = '.$_POST['id'].'
');
if(!($row = $result->fetch_assoc()))
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A diákot törölték az adatbázisból',
	)));
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'name' => $row['name'],
	'class' => $row['class'],
));

$conn->close();

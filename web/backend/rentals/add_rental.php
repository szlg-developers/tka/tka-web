<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(1, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'book_code', 'student_id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['book_code'], 'string', 32, 1);
check_param($_POST['student_id'], 'integer');

// Kölcsönzés felvétele
$query = $conn->prepare('
	INSERT INTO `rentals` (`book_code`, `student_id`, `date`)
	SELECT `books`.`code`, `students`.`id`, NOW() FROM `books`
	INNER JOIN `booktypes` ON `booktypes`.`id` = `books`.`type_id`
	INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
	INNER JOIN `students`
	INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	WHERE `bookcategories`.`school_id` = ? AND `books`.`code` = ?
		AND `classes`.`school_id` = `bookcategories`.`school_id` AND `students`.`id` = ?
');
$query->bind_param('isi', $_POST['school_id'], $_POST['book_code'], $_POST['student_id']);
$query->execute();
$query->close();

echo json_encode(array
(
	'success' => true,
));

$conn->close();

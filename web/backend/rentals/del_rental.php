<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(1, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'book_code');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['book_code'], 'string', 32, 1);

// Kölcsönzés törlése
$query = $conn->prepare('
	DELETE `rentals` FROM `rentals`
	INNER JOIN `students` ON `students`.`id` = `rentals`.`student_id`
	INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	WHERE `classes`.`school_id` = ? AND `rentals`.`book_code` = ?
');
$query->bind_param('is', $_POST['school_id'], $_POST['book_code']);
$query->execute();
$query->close();

echo json_encode(array
(
	'success' => true,
));

$conn->close();

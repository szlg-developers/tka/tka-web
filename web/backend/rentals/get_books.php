<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(1, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'type_id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['type_id'], 'integer');

// Egy adott könyvtípushoz tartozó könyvek lekérése
$books = [];

$result = $conn->query('
	SELECT `books`.`code`, `rentals`.`student_id` IS NULL AS `available` FROM `books`
	INNER JOIN `booktypes` ON `booktypes`.`id` = `books`.`type_id`
	INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
	LEFT JOIN `rentals` ON `rentals`.`book_code` = `books`.`code`
	WHERE `bookcategories`.`school_id` = '.$_POST['school_id'].' AND `booktypes`.`id` = '.$_POST['type_id'].'
');
while($row = $result->fetch_assoc())
{
	$books[] = array
	(
		'code' => $row['code'],
		'available' => $row['available'],
	);
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => $books,
));

$conn->close();

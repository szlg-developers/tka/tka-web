<?php

header('Content-type: application/json');
include 'includes/keycheck.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'class_id');
check_param($_POST['class_id'], 'integer');

// Adott osztály névsorának lekérése
$student_id = [];
$student_name = [];

$query = $conn->prepare('
	SELECT `students`.`id`, `students`.`name` FROM `students`
	INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	WHERE `classes`.`school_id` = ? AND `classes`.`id` = ?
	ORDER BY `students`.`name`
');
$query->bind_param('ii', $school_id, $_POST['class_id']);
$query->execute();
$query->bind_result($id, $name);
while($query->fetch())
{
	$student_id[] = $id;
	$student_name[] = $name;
}
$query->close();

echo json_encode(array
(
	'success' => true,
	'diak' => $student_name,
	'diak_id' => $student_id,
));

$conn->close();

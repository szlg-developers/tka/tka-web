<?php

header('Content-type: application/json');
include 'includes/keycheck.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'student_id', 'book_codes');
check_param($_POST['student_id'], 'integer');
check_param($_POST['book_codes'], 'json');
check_param($_POST['book_codes'], 'array');
foreach($_POST['book_codes'] as $value)
	check_param($value, 'string', 32, 1);

// Tranzakció indítása
$conn->autocommit(false);

// Kölcsönzések rögzítése
$db = 0;
$query = $conn->prepare('
	INSERT INTO `rentals` (`book_code`, `student_id`, `date`)
	SELECT `books`.`code`, `students`.`id`, NOW() FROM `books`
	INNER JOIN `booktypes` ON `booktypes`.`id` = `books`.`type_id`
	INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
	INNER JOIN `students`
	INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	WHERE `bookcategories`.`school_id` = ? AND `books`.`code` = ?
		AND `classes`.`school_id` = `bookcategories`.`school_id` AND `students`.`id` = ?
');
foreach($_POST['book_codes'] as $code)
{
	$query->bind_param('isi', $school_id, $code, $_POST['student_id']);
	$query->execute();

	// Hiba, ha nem sikerült rögzíteni (nem létező könyv vagy diák)
	if($conn->affected_rows != 1)
	{
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Nem sikerült rögzíteni az adatokat',
		)));
	}
	++$db;
}
$query->close();

// Véglegesítése
$conn->commit();

echo json_encode(array
(
	'success' => true,
	'count' => $db,
));

$conn->close();

<?php

if(empty($_POST['appkey']))
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Hitelesítés szükséges',
	)));

include '../../../config.php';

$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, 'tka');

// Kulcs használatának logolása (feltéve, hogy érvényes a kulcs)
$query = $conn->prepare('UPDATE `appkeys` SET `lastused` = NOW() WHERE `key` = ? AND `registered` AND `enabled`');
$query->bind_param('s', $_POST['appkey']);
$query->execute();
$query->close();

// Kulcshoz rendelt iskola lekérése (érvényesség ellenőrzése)
$query = $conn->prepare('SELECT `school_id` FROM `appkeys` WHERE `key` = ? AND `registered` AND `enabled`');
$query->bind_param('s', $_POST['appkey']);
$query->execute();
$query->bind_result($school_id);
if(!$query->fetch())
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Érvénytelen kulcs',
	)));
$query->close();

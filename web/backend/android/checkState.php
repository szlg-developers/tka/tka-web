<?php

header('Content-type: application/json');
include 'includes/keycheck.php';

// Iskola adatainak lekérése
$query = $conn->prepare('
	SELECT `schools`.`name`, `appkeys`.`name` FROM `appkeys`
	INNER JOIN `schools` ON `schools`.`id` = `appkeys`.`school_id`
	WHERE `appkeys`.`key` = ?
');
$query->bind_param('s', $_POST['appkey']);
$query->execute();
$query->bind_result($school_name, $appkey_name);
if(!$query->fetch())
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A kért iskola nem található',
	)));
$query->close();

echo json_encode(array
(
	'success' => true,
	'tanker' => INSTANCE_NAME,
	'suli' => $school_name,
	'keyname' => $appkey_name,
));

<?php

if(empty($_GET['token']))
	exit;

header('Content-type: application/json');

include '../../../config.php';

$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, 'tka');

// Tábla zárolása (konfliktusok elkerülése végett)
$conn->query('LOCK TABLES `appkeys` WRITE');

// Alkalmazáskulcs regisztrálása (alkalmazáshoz rendelése)
$query = $conn->prepare('SELECT `id`, `key` FROM `appkeys` WHERE `regist_token` = ? AND NOT `registered`');
$query->bind_param('s', $_GET['token']);
$query->execute();
$query->bind_result($id, $key);
if(!$query->fetch())
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Érvénytelen regisztrációs token',
	)));
}
$query->close();

// Regisztrálás rögzítése
$conn->query('UPDATE `appkeys` SET `registered` = 1 WHERE `id` = '.$id);

// Tábla zárolásának feloldása
$conn->query('UNLOCK TABLES');

$conn->close();

echo json_encode(array
(
	'success' => true,
	'key' => $key,
));

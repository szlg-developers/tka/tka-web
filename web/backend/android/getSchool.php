<?php

header('Content-type: application/json');
include 'includes/keycheck.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'code');
check_param($_POST['code'], 'string', 32, 1);

// Kért könyv címének lekérése + érvényes-e a beolvasott vonalkód?
$query = $conn->prepare('
	SELECT `booktypes`.`title` FROM `books`
	INNER JOIN `booktypes` ON `booktypes`.`id` = `books`.`type_id`
	INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
	WHERE `bookcategories`.`school_id` = ? AND `books`.`code` = ?
');
$query->bind_param('is', $school_id, $_POST['code']);
$query->execute();
$query->bind_result($title);
if(!$query->fetch())
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A kért könyv nem található',
	)));
$query->close();

echo json_encode(array
(
	'success' => true,
	'title' => $title,
));

$conn->close();

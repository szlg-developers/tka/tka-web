<?php

header('Content-type: application/json');
include 'includes/keycheck.php';

// Osztályok lekérése
$class_id = [];
$class_name = [];

$result = $conn->query('SELECT `id`, `name` FROM `classes` WHERE `school_id` = '.$school_id);
while($row = $result->fetch_assoc())
{
	$class_id[] = $row['id'];
	$class_name[] = $row['name'];
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'osztály' => $class_name,
	'osztály_id' => $class_id,
));

$conn->close();

<?php

session_start();
if(!empty($_SESSION['user']['id']) || !isset($_POST['username']) || !isset($_POST['password']) || !isset($_POST['school']))
{
	exit;
}

include '../../config.php';

// Utoljára kiválasztott iskola mentése
setcookie('lastused_school', intval($_POST['school']), strtotime('+90 days'), WEB_ROOT.'/');

// CSRF token meglétének és hitelességének ellenőrzése
if(empty($_POST['csrf']) || empty($_SESSION['csrf']) || $_POST['csrf'] != $_SESSION['csrf'])
{
	$_SESSION['login_error'] = 'CSRF hiba, próbálja újra';
	if(!empty($_GET['continue']))
		header('Location: ../bejelentkezes?continue='.urlencode($_GET['continue']));
	else
		header('Location: ../bejelentkezes');
	exit;
}

$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, 'tka');

// Felhasználó adatainak lekérése felhasználónév alapján
$query = $conn->prepare('
	SELECT `users`.`id`, `users`.`name`, `users`.`password`, `users`.`level`, `schools`.`code` FROM `users`
	LEFT JOIN `schools` ON `schools`.`id` = `users`.`school_id`
	WHERE `users`.`username` = ? AND `schools`.`id` = ?
');
$query->bind_param('si', $_POST['username'], $_POST['school']);
$query->execute();
$query->bind_result($id, $name, $password, $level, $school);

// A felhasználó létezésének és a jelszó érvényességének ellenőrzése
if(!$query->fetch() || !password_verify($_POST['password'], $password))
{
	$_SESSION['login_error'] = 'Hibás felhasználónév vagy jelszó';
	if(!empty($_GET['continue']))
		header('Location: ../bejelentkezes?continue='.urlencode($_GET['continue']));
	else
		header('Location: ../bejelentkezes');
	exit;
}
$query->close();

$conn->close();

// Felhasználó bejelentkeztetése
$_SESSION['user'] = array
(
	'id' => $id,
	'username' => $_POST['username'],
	'name' => $name,
	'password' => $password,
	'level' => $level,
	'school' => $school,
);

// Átirányítás
if(!empty($_GET['continue']))
	header('Location: '.ROOT_URL.'/'.$_GET['continue']);
else
	header('Location: ../');

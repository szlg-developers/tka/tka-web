<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'category_id', 'title', 'codes');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['category_id'], 'integer');
check_param($_POST['title'], 'string', 255, 1, true);
check_param($_POST['codes'], 'json');
check_param($_POST['codes'], 'array');
foreach($_POST['codes'] as $code)
{
	check_param($code, 'string', 32, 1, true);
}

// Könyvtípus hozzáadása
$query = $conn->prepare('
	INSERT INTO `booktypes` (`category_id`, `title`, `date`)
	SELECT `bookcategories`.`id`, ?, NOW() FROM `bookcategories`
	WHERE `bookcategories`.`id` = ? AND `bookcategories`.`school_id` = ?
');
$query->bind_param('sii', $_POST['title'], $_POST['category_id'], $_POST['school_id']);
$query->execute();
$query->close();

$type_id = $conn->insert_id;

// Könyvek hozzáadása a könyvtípushoz
$query = $conn->prepare('INSERT INTO `books` (`type_id`, `code`, `date`) VALUES (?, ?, NOW())');
foreach($_POST['codes'] as $code)
{
	$query->bind_param('is', $type_id, $code);
	$query->execute();
}
$query->close();

echo json_encode(array
(
	'success' => true,
));

$conn->close();

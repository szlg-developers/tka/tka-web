<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id', 'changes');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');
check_param($_POST['changes'], 'json');
check_param($_POST['changes'], 'array');
foreach($_POST['changes'] as $key => $value)
{
	switch($key)
	{
		case 'category_id':
			check_param($value, 'integer');
			continue 2;
		case 'title':
			check_param($value, 'string', 255, 1, true);
			continue 2;
		case 'codes':
			check_param($value, 'array');
			isset_param($value, 'deleted', 'new');
			check_param($value['deleted'], 'array');
			check_param($value['new'], 'array');
			foreach($value['deleted'] as $code)
				check_param($code, 'string', 32, 1);
			foreach($value['new'] as $code)
				check_param($code, 'string', 32, 1, true);
			continue 2;
		default:
			wrong_param();
	}
}


// Módosító lekérdezés előkészítése (csak azokat frissítjük, amik módosultak)
$query_sets = '';
$query_types = '';
$query_params = [];

if(isset($_POST['changes']['category_id']))
{
	$query_sets .= '`booktypes`.`category_id` = ?, ';
	$query_types .= 'i';
	$query_params[] = $_POST['changes']['category_id'];
}
if(isset($_POST['changes']['title']))
{
	$query_sets .= '`booktypes`.`title` = ?, ';
	$query_types .= 's';
	$query_params[] = $_POST['changes']['title'];
}

// Ha van változás...
if($query_sets != '')
{
	// Lekérdezés paramétereinek véglegesítése
	$func_params = [ $query_types ];
	$count = count($query_params);
	for($i = 0; $i < $count; ++$i)
		$func_params[] = &$query_params[$i];

	// Könyvtípus adatainak módosítása
	$query = $conn->prepare('
		UPDATE `booktypes`
		INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
		SET '.substr($query_sets, 0, strlen($query_sets) - 2).'
		WHERE `bookcategories`.`school_id` = '.$_POST['school_id'].' AND `booktypes`.`id` = '.$_POST['id'].'
	');
	call_user_func_array(array($query, 'bind_param'), $func_params);
	$query->execute();
	$query->close();
}

if(count($_POST['changes']['codes']['new']) > 0)
{
	// Új könyvkódok hozzáadása
	$query = $conn->prepare('
		INSERT INTO `books` (`type_id`, `code`, `date`)
		SELECT `booktypes`.`id`, ?, NOW() FROM `booktypes`
		INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
		WHERE `booktypes`.`id` = ? AND `bookcategories`.`school_id` = ?
	');
	foreach($_POST['changes']['codes']['new'] as $code)
	{
		$query->bind_param('sii', $code, $_POST['id'], $_POST['school_id']);
		$query->execute();
	}
	$query->close();
}

$count = count($_POST['changes']['codes']['deleted']) > 0;
if($count)
{
	// Törlő lekérdezés előkészítése
	$query_in = '';
	$query_types = '';
	$query_params = [];

	foreach($_POST['changes']['codes']['deleted'] as $code)
	{
		$query_in .= '?, ';
		$query_types .= 's';
		$query_params[] = $code;
	}

	// Lekérdezés paramétereinek véglegesítése
	$func_params = [ $query_types ];
	$count = count($query_params);
	for($i = 0; $i < $count; ++$i)
		$func_params[] = &$query_params[$i];

	// Régi könyvkódok törlése
	$query = $conn->prepare('
		DELETE `books` FROM `books`
		INNER JOIN `booktypes` ON `booktypes`.`id` = `books`.`type_id`
		INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
		WHERE `bookcategories`.`school_id` = '.$_POST['school_id'].' AND `books`.`code` IN ('.substr($query_in, 0, strlen($query_in) - 2).')');
	call_user_func_array(array($query, 'bind_param'), $func_params);
	$query->execute();
	$query->close();
}

echo json_encode(array
(
	'success' => true,
));

$conn->close();

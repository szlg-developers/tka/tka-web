<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Könyvtípus lekérése
$result = $conn->query('
	SELECT `booktypes`.`category_id`, `booktypes`.`title` FROM `booktypes`
	INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
	WHERE `bookcategories`.`school_id` = '.$_POST['school_id'].' AND `booktypes`.`id` = '.$_POST['id'].'
');
if(!($row = $result->fetch_assoc()))
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A könyvtípust törölték az adatbázisból',
	)));
}
$result->close();

$type = array
(
	'id' => $_POST['id'],
	'category_id' => $row['category_id'],
	'title' => $row['title'],
	'codes' => [],
	'rentals' => array(),
);

// A könyvtípushoz tartozó könyvek lekérése
$result = $conn->query('
	SELECT `books`.`code` AS `code`, `students`.`name` AS `name`, `classes`.`name` AS `class` FROM `books`
	LEFT JOIN `rentals` ON `rentals`.`book_code` = `books`.`code`
	LEFT JOIN `students` ON `students`.`id` = `rentals`.`student_id`
	LEFT JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	WHERE `books`.`type_id` = '.$_POST['id'].'
');
while($row = $result->fetch_assoc())
{
	$type['codes'][] = $row['code'];
	if($row['name'] !== null)
	{
		$type['rentals'][$row['code']] = array
		(
			'name' => $row['name'],
			'class' => $row['class'],
		);
	}
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => $type,
));

$conn->close();

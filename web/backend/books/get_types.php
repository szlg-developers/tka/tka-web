<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(1, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'category_id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['category_id'], 'integer');

// Könyvtípusok lekérése
$types = [];

$result = $conn->query('
	SELECT `booktypes`.`id`, `booktypes`.`title`, COUNT(DISTINCT `books`.`code`) AS `book_db`, COUNT(`rentals`.`book_code`) AS `rental_db` FROM `booktypes`
	INNER JOIN `bookcategories` ON `bookcategories`.`id` = `booktypes`.`category_id`
	LEFT JOIN `books` ON `books`.`type_id` = `booktypes`.`id`
	LEFT JOIN `rentals` ON `rentals`.`book_code` = `books`.`code`
	WHERE `bookcategories`.`school_id` = '.$_POST['school_id'].' AND `bookcategories`.`id` = '.$_POST['category_id'].'
	GROUP BY `booktypes`.`id`, `booktypes`.`title`
	ORDER BY `booktypes`.`title`
');
while($row = $result->fetch_assoc())
{
	$types[] = array
	(
		'id' => $row['id'],
		'title' => $row['title'],
		'book_db' => $row['book_db'],
		'rental_db' => $row['rental_db'],
	);
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'level' => $_SESSION['user']['level'],
	'data' => $types,
));

$conn->close();

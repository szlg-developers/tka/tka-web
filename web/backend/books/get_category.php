<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Könyvkategória lekérése
$result = $conn->query('
	SELECT `bookcategories`.`name`, COUNT(DISTINCT `booktypes`.`id`) AS `booktype_db`,
		COUNT(DISTINCT `books`.`code`) AS `book_db`, COUNT(`rentals`.`book_code`) AS `rental_db` FROM `bookcategories`
	LEFT JOIN `booktypes` ON `booktypes`.`category_id` = `bookcategories`.`id`
	LEFT JOIN `books` ON `books`.`type_id` = `booktypes`.`id`
	LEFT JOIN `rentals` ON `rentals`.`book_code` = `books`.`code`
	WHERE `bookcategories`.`school_id` = '.$_POST['school_id'].' AND `bookcategories`.`id` = '.$_POST['id'].'
	GROUP BY `bookcategories`.`id`, `bookcategories`.`name`
');
if(!($row = $result->fetch_assoc()))
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A kategóriát törölték az adatbázisból',
	)));
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => array
	(
		'id' => $_POST['id'],
		'name' => $row['name'],
		'booktype_db' => $row['booktype_db'],
		'book_db' => $row['book_db'],
		'rental_db' => $row['rental_db'],
	),
));

$conn->close();

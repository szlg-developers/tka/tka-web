<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'category_id');
isset_param($_FILES, 'file');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['category_id'], 'integer');
check_param($_FILES['file'], 'file', 'text/plain', 'text/tab-separated-values');

// Tranzakció előkészítése
$conn->autocommit(false);

$line_number = 0;
$db = 0;

// Fájl megnyitása
$file = fopen($_FILES['file']['tmp_name'], 'r');
while(($line = fgets($file)) !== false)
{
	++$line_number;

	// Üres sorok kihagyása
	$line = trim($line);
	if($line == '')
		continue;

	// Oszlopszélesség ellenőrzése
	$columns = explode("\t", $line);
	if(count($columns) < 2)
	{
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Sikertelen importálás: '.$line_number.'. sor: hiányzó oszlop, vagy nincs tabulátorral elválasztva',
		)));
	}

	// Könyv címének ellenőrzése (1. oszlop)
	$columns[0] = trim($columns[0]);
	if(strlen($columns[0]) < 1 || strlen($columns[0]) > 255)
	{
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Sikertelen importálás: '.$line_number.'. sor: a könyv címe nincs megadva, vagy túl hosszú',
		)));
	}

	// Könyvtípus felvétele
	$query = $conn->prepare('
		INSERT INTO `booktypes` (`category_id`, `title`, `date`)
		SELECT `bookcategories`.`id`, ?, NOW() FROM `bookcategories`
		WHERE `bookcategories`.`id` = ? AND `bookcategories`.`school_id` = ?
	');
	$query->bind_param('sii', $columns[0], $_POST['category_id'], $_POST['school_id']);
	if(!$query->execute())
	{
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Sikertelen importálás: '.$line_number.'. sor: nem lehet hozzáadni a könyvet a választott kategóriához',
		)));
	}
	$type_id = $conn->insert_id;
	$query->close();

	// Könyvek felvétele
	$query = $conn->prepare('INSERT INTO `books` (`code`, `type_id`, `date`) VALUES (?, ?, NOW())');

	// Könyvkódok szétdarabolása
	$columns[1] = trim($columns[1]);
	$codes = explode(',', $columns[1]);
	foreach($codes as $range)
	{
		// Egyetlen kód vagy egy sorozat?
		$range = explode('-', $range);
		if(count($range) == 1)
		{
			// Egyetlen kód
			// Kód ellenőrzése
			$code = trim($range[0]);
			if(strlen($code) < 1 || strlen($code) > 32)
			{
				exit(json_encode(array
				(
					'success' => false,
					'message' => 'Sikertelen importálás: '.$line_number.'. sor: szintaktikai hiba a kódok felsorolásánál',
				)));
			}
			// Könyv felvétele
			$query->bind_param('si', $code, $type_id);
			if(!$query->execute())
			{
				exit(json_encode(array
				(
					'success' => false,
					'message' => 'Sikertelen importálás: '.$line_number.'. sor: nem lehet hozzáadni a(z) '.$code.' kódú könyvet a választott kategóriához',
				)));
			}
			++$db;
		}
		else
		{
			// Kódsorozat
			// Kezdő- és zárótag ellenőrzése
			if(strlen($range[0]) > 32 || strlen($range[1]) > 32 || strlen($range[0]) != strlen($range[1]))
			{
				exit(json_encode(array
				(
					'success' => false,
					'message' => 'Sikertelen importálás: '.$line_number.'. sor: szintaktikai hiba a kódok felsorolásánál',
				)));
			}
			// Könyvkódok szám-értékének kinyerése
			preg_match('/(\d+)(.*)/', strrev($range[0]), $matches_start);
			preg_match('/(\d+)(.*)/', strrev($range[1]), $matches_end);
			$start = count($matches_start) == 3 ? intval(strrev($matches_start[1])) : null;
			$end = count($matches_end) == 3 ? intval(strrev($matches_end[1])) : null;
			$prefix = $matches_start[2] === $matches_end[2] ? strrev($matches_start[2]) : null;
			if($start === null || $end === null || $prefix === null || $start > $end)
			{
				exit(json_encode(array
				(
					'success' => false,
					'message' => 'Sikertelen importálás: '.$line_number.'. sor: szintaktikai hiba a kódok felsorolásánál',
				)));
			}
			// Könyvek felvétele
			for($i = $start; $i <= $end; ++$i)
			{
				// Könyvkód összerakása (prefix + szám-érték)
				$code = strval($i);
				while(strlen($code) < strlen($range[0]) - strlen($prefix))
					$code = '0'.$code;
				$code = $prefix.$code;
				// Könyv felvétele
				$query->bind_param('si', $code, $type_id);
				if(!$query->execute())
				{
					exit(json_encode(array
					(
						'success' => false,
						'message' => 'Sikertelen importálás: '.$line_number.'. sor: nem lehet hozzáadni a(z) '.$code.' kódú könyvet a választott kategóriához',
					)));
				}
				++$db;
			}
		}
	}

	$query->close();
}
fclose($file);

// Véglegesítés
$conn->commit();

echo json_encode(array
(
	'success' => true,
	'db' => $db,
));

$conn->close();

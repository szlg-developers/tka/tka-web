<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(3, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Alkalmazáskulcs lekérése
$result = $conn->query('SELECT SHA2(`key`, 256) AS `fingerprint`, `name`, `lastused`, `registered`, `regist_token` FROM `appkeys` WHERE `school_id` = '.$_POST['school_id'].' AND `id` = '.$_POST['id']);
if(!($row = $result->fetch_assoc()))
{
	$result->close();
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A kért kulcs nem található',
	)));
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => array
	(
		'id' => $_POST['id'],
		'fingerprint' => $row['fingerprint'],
		'name' => $row['name'],
		'lastused' => $row['lastused'],
		'registered' => boolval($row['registered']),
		'qrcode_url' => $row['registered'] ? '' : ROOT_URL.'/backend/android/registKey.php?token='.$row['regist_token'],
	),
));

$conn->close();

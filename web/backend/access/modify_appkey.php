<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(3, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id', 'name');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');
check_param($_POST['name'], 'string', 255, 1, true);

// Alkalmazáskulcs módosítása
$query = $conn->prepare('UPDATE `appkeys` SET `name` = ? WHERE `school_id` = '.$_POST['school_id'].' AND `id` = '.$_POST['id']);
$query->bind_param('s', $_POST['name']);
$query->execute();
$query->close();

echo json_encode(array
(
	'success' => true,
));

$conn->close();

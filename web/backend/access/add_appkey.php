<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(3, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id');
check_param($_POST['school_id'], 'integer', null, 2);

// Alkalmazáskulcs adatainak generálása/felvétele
$name = 'Névtelen kulcs';
$token = bin2hex(random_bytes(16));
$key = bin2hex(random_bytes(128));

// Alkalmazáskulcs hozzáadása
$query = $conn->prepare('INSERT INTO `appkeys` (`name`, `key`, `regist_token`, `school_id`, `date`) VALUES (?, ?, ?, ?, NOW())');
$query->bind_param('sssi', $name, $key, $token, $_POST['school_id']);
if(!$query->execute())
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Nem sikerült létrehozni a kulcsot',
	)));
}
$query->close();

echo json_encode(array
(
	'success' => true,
	'id' => $conn->insert_id,
	'qrcode_url' => ROOT_URL.'/backend/android/registKey.php?token='.$token,
	'name' => $name,
	'fingerprint' => hash('sha256', $key),
));

$conn->close();

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(3, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'username', 'password', 'level');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['username'], 'string', 255, 1, true);
check_param($_POST['name'], 'string', 255, 0, true);
check_param($_POST['password'], 'string', null, 1);
check_param($_POST['level'], 'integer', 3, 1);

// Jelszó titkosítása
$_POST['password'] = password_hash($_POST['password'], PASSWORD_BCRYPT);

// Felhasználó hozzáadása
$query = $conn->prepare('INSERT INTO `users` (`username`, `password`, `name`, `level`, `school_id`, `date`) VALUES (?, ?, ?, ?, ?, NOW())');
$query->bind_param('sssii', $_POST['username'], $_POST['password'], $_POST['name'], $_POST['level'], $_POST['school_id']);
$query->execute();
$query->close();

echo json_encode(array
(
	'success' => true,
	'valid' => $conn->insert_id != 0,
));

$conn->close();

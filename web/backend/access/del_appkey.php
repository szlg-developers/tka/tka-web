<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(3, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Alkalmazáskulcs törlése
$conn->query('DELETE FROM `appkeys` WHERE `school_id` = '.$_POST['school_id'].' AND `id` = '.$_POST['id']);

echo json_encode(array
(
	'success' => true,
));

$conn->close();

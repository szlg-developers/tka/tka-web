<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(3, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Saját fiók törlésének tiltása
if($_POST['id'] == $_SESSION['user']['id'])
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Saját fiókot nem lehet törölni',
	)));
}

// Felhasználó törlése
$conn->query('DELETE FROM `users` WHERE `school_id` = '.$_POST['school_id'].' AND `id` = '.$_POST['id']);

echo json_encode(array
(
	'success' => true,
));

$conn->close();

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(3, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id', 'changes');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');
check_param($_POST['changes'], 'json');
check_param($_POST['changes'], 'array', null, 1);
foreach($_POST['changes'] as $key => $value)
{
	switch($key)
	{
		case 'username':
			check_param($value, 'string', 255, 1, true);
			continue 2;
		case 'name':
			check_param($value, 'string', 255, 0, true);
			continue 2;
		case 'password':
			check_param($value, 'string', null, 1);
			continue 2;
		case 'level':
			check_param($value, 'integer', 3, 1);
			continue 2;
		default:
			wrong_param();
	}
}


// Módosító lekérdezés előkészítése (csak azokat frissítjük, amik módosultak)
$query_sets = '';
$query_types = '';
$query_params = [];

if(isset($_POST['changes']['username']))
{
	$query_sets .= '`username` = ?, ';
	$query_types .= 's';
	$query_params[] = $_POST['changes']['username'];
}
if(isset($_POST['changes']['name']))
{
	$query_sets .= '`name` = ?, ';
	$query_types .= 's';
	$query_params[] = $_POST['changes']['name'];
}
if(isset($_POST['changes']['password']))
{
	$query_sets .= '`password` = ?, ';
	$query_types .= 's';
	$query_params[] = password_hash($_POST['changes']['password'], PASSWORD_BCRYPT);
}
if(isset($_POST['changes']['level']) && $_POST['id'] != $_SESSION['user']['id'])
{
	$query_sets .= '`level` = ?, ';
	$query_types .= 'i';
	$query_params[] = $_POST['changes']['level'];
}

// Hiba, ha semmilyen változtatás nem lett elküldve
if($query_sets == '')
{
	missing_param();
}

// Lekérdezés paramétereinek véglegesítése
$func_params = [ $query_types ];
$count = count($query_params);
for($i = 0; $i < $count; ++$i)
	$func_params[] = &$query_params[$i];

// Felhasználó módosítása
$query = $conn->prepare('UPDATE `users` SET '.substr($query_sets, 0, strlen($query_sets) - 2).' WHERE `school_id` = '.$_POST['school_id'].' AND `id` = '.$_POST['id']);
call_user_func_array(array($query, 'bind_param'), $func_params);
$query->execute();

echo json_encode(array
(
	'success' => true,
	'valid' => $conn->affected_rows == 1,
	'relogin' => $_POST['id'] == $_SESSION['user']['id'],
));

$query->close();

$conn->close();

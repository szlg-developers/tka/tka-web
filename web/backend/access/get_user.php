<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(3, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Felhasználó lekérése
$result = $conn->query('SELECT `username`, `name`, `level` FROM `users` WHERE `school_id` = '.$_POST['school_id'].' AND `id` = '.$_POST['id']);
if(!($row = $result->fetch_assoc()))
{
	$result->close();
	$conn->close();
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A kért felhasználó nem található',
	)));
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => array
	(
		'id' => $_POST['id'],
		'username' => $row['username'],
		'name' => $row['name'],
		'level' => $row['level'],
		'user_id' => $_SESSION['user']['id'],
	),
));

$conn->close();

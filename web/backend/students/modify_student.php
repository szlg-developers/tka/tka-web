<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id', 'changes');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');
check_param($_POST['changes'], 'json');
check_param($_POST['changes'], 'array', null, 1);
foreach($_POST['changes'] as $key => $value)
{
	switch($key)
	{
		case 'class_id':
			check_param($value, 'integer');
			continue 2;
		case 'name':
			check_param($value, 'string', 255, 1, true);
			continue 2;
		case 'omaz':
			check_param($value, 'string', 11, 11, true);
			continue 2;
		default:
			wrong_param();
	}
}

// Módosító lekérdezés előkészítése (csak azokat frissítjük, amik módosultak)
$query_sets = '';
$query_types = '';
$query_params = [];

if(isset($_POST['changes']['class_id']))
{
	$query_sets .= '`students`.`class_id` = ?, ';
	$query_types .= 'i';
	$query_params[] = $_POST['changes']['class_id'];
}
if(isset($_POST['changes']['name']))
{
	$query_sets .= '`students`.`name` = ?, ';
	$query_types .= 's';
	$query_params[] = $_POST['changes']['name'];
}
if(isset($_POST['changes']['omaz']))
{
	$query_sets .= '`students`.`omaz` = ?, ';
	$query_types .= 's';
	$query_params[] = $_POST['changes']['omaz'];
}

// Hiba, ha semmilyen változtatás nem lett elküldve
if($query_sets == '')
{
	missing_param();
}

// Lekérdezés paramétereinek véglegesítése
$func_params = [ $query_types ];
$count = count($query_params);
for($i = 0; $i < $count; ++$i)
	$func_params[] = &$query_params[$i];

// Diák módosítása
$query = $conn->prepare('
	UPDATE `students`
	INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	SET '.substr($query_sets, 0, strlen($query_sets) - 2).'
	WHERE `classes`.`school_id` = '.$_POST['school_id'].' AND `students`.`id` = '.$_POST['id'].'
');
call_user_func_array(array($query, 'bind_param'), $func_params);
$query->execute();

echo json_encode(array
(
	'success' => true,
	'valid' => $conn->affected_rows == 1,
));

$query->close();

$conn->close();

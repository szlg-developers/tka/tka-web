<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id');
check_param($_POST['school_id'], 'integer', null, 2);

// Osztályok lekérése
$classes = [];

$result = $conn->query('SELECT `id`, `name` FROM `classes` WHERE `school_id` = '.$_POST['school_id']);
while($row = $result->fetch_assoc())
{
	$classes[] = array
	(
		'value' => $row['id'],
		'name' => $row['name'],
	);
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => $classes,
));

$conn->close();

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'class_id', 'name', 'omaz');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['class_id'], 'integer');
check_param($_POST['name'], 'string', 255, 1, true);
check_param($_POST['omaz'], 'string', 11, 11, true);

// Diák hozzáadása
$query = $conn->prepare('
	INSERT INTO `students` (`name`, `omaz`, `class_id`, `date`)
	SELECT ?, ?, `classes`.`id`, NOW() FROM `classes`
	WHERE `classes`.`id` = ? AND `classes`.`school_id` = ?
');
$query->bind_param('ssii', $_POST['name'], $_POST['omaz'], $_POST['class_id'], $_POST['school_id']);
$query->execute();
$query->close();

echo json_encode(array
(
	'success' => true,
	'valid' => $conn->insert_id != 0,
));

$conn->close();

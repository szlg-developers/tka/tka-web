<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'class_id');
isset_param($_FILES, 'file');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['class_id'], 'integer');
check_param($_FILES['file'], 'file', 'text/plain', 'text/tab-separated-values');

// Tranzakció előkészítése
$conn->autocommit(false);

$query = $conn->prepare('
	INSERT INTO `students` (`omaz`, `class_id`, `name`, `date`)
	SELECT ?, `classes`.`id`, ?, NOW() FROM `classes`
	WHERE `classes`.`id` = ? AND `classes`.`school_id` = ?
');

$line_number = 0;
$db = 0;

// Fájl megnyitása
$file = fopen($_FILES['file']['tmp_name'], 'r');
while(($line = fgets($file)) !== false)
{
	++$line_number;

	// Üres sorok kihagyása
	$line = trim($line);
	if($line == '')
		continue;

	// Oszlopok ellenőrzése
	$columns = explode("\t", $line);
	if(count($columns) < 2)
	{
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Sikertelen importálás: '.$line_number.'. sor: hiányzó oszlop, vagy nincs tabulátorral elválasztva',
		)));
	}

	// OM azonosító ellenőrzése (1. oszlop)
	$columns[0] = trim($columns[0]);
	if(strlen($columns[0]) != 11)
	{
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Sikertelen importálás: '.$line_number.'. sor: a diák OM azonosítója nincs megadva, vagy nem 11 karakter hosszú',
		)));
	}

	// Diák nevének ellenőrzése (2. oszlop)
	$columns[1] = trim($columns[1]);
	if(strlen($columns[1]) < 1 || strlen($columns[1]) > 255)
	{
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Sikertelen importálás: '.$line_number.'. sor: a diák neve nincs megadva, vagy túl hosszú',
		)));
	}

	// Diák hozzáadása
	$query->bind_param('ssii', $columns[0], $columns[1], $_POST['class_id'], $_POST['school_id']);
	if(!$query->execute())
	{
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Sikertelen importálás: '.$line_number.'. sor: nem lehet hozzáadni a diákot a választott osztályhoz',
		)));
	}
	++$db;
}
fclose($file);

$query->close();

// Véglegesítés
$conn->commit();

echo json_encode(array
(
	'success' => true,
	'db' => $db,
));

$conn->close();

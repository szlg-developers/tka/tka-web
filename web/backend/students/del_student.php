<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Diák törlése
$conn->query('
	DELETE `students` FROM `students`
	INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	WHERE `classes`.`school_id` = '.$_POST['school_id'].' AND `students`.`id` = '.$_POST['id']
);

echo json_encode(array
(
	'success' => true,
));

$conn->close();

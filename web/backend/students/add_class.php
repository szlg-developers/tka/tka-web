<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'name');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['name'], 'string', 255, 1, true);

// Osztály hozzáadása
$query = $conn->prepare('INSERT INTO `classes` (`name`, `school_id`, `date`) VALUES (?, ?, NOW())');
$query->bind_param('si', $_POST['name'], $_POST['school_id']);
$query->execute();
$query->close();

echo json_encode(array
(
	'success' => true,
));

$conn->close();

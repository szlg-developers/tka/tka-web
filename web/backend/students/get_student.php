<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Egy adott diák adatainak lekérése
$result = $conn->query('
	SELECT `students`.`name`, `students`.`omaz`, `students`.`class_id` FROM `students`
	INNER JOIN `classes` ON `classes`.`id` = `students`.`class_id`
	WHERE `classes`.`school_id` = '.$_POST['school_id'].' AND `students`.`id` = '.$_POST['id'].'
');
if(!($row = $result->fetch_assoc()))
{
	$result->close();
	$conn->close();
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A kért diák nem található',
	)));
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => array
	(
		'id' => $_POST['id'],
		'name' => $row['name'],
		'omaz' => $row['omaz'],
		'class_id' => $row['class_id'],
	),
));

$conn->close();

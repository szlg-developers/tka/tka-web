<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(2, !empty($_POST['school_id']) ? $_POST['school_id'] : 1);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'school_id', 'id');
check_param($_POST['school_id'], 'integer', null, 2);
check_param($_POST['id'], 'integer');

// Egy adott osztály adatainak lekérése
$result = $conn->query('SELECT `classes`.`name`, COUNT(`students`.`id`) AS `student_db` FROM `classes`
	LEFT JOIN `students` ON `students`.`class_id` = `classes`.`id`
	WHERE `classes`.`school_id` = '.$_POST['school_id'].' AND `classes`.`id` = '.$_POST['id'].'
	GROUP BY `classes`.`id`, `classes`.`name`
');
if(!($row = $result->fetch_assoc()))
{
	$result->close();
	$conn->close();
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A kért osztály nem található',
	)));
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => array
	(
		'id' => $_POST['id'],
		'name' => $row['name'],
		'student_db' => $row['student_db'],
	),
));

$conn->close();

<?php

function isset_param($array)
{
	$count = func_num_args();
	for($i = 1; $i < $count; ++$i)
	{
		if(!isset($array[func_get_arg($i)]))
		{
			missing_param();
		}
	}
}

function check_param(&$var, $type)
{
	$args = func_get_args();
	switch($type)
	{
		case 'string':
			if(gettype($var) == 'string')
			{
				if(isset($args[4]) && $args[4])
					$var = trim($var);
				$length = strlen($var);
				if(!((!empty($args[2]) && $args[2] < $length) || (!empty($args[3]) && $args[3] > $length)))
					return;
			}
			break;
		case 'integer':
			if(gettype($var) == 'string')
			{
				$var = intval($var);
			}
			if(gettype($var) == 'integer')
			{
				if(!((!empty($args[2]) && $args[2] < $var) || (!empty($args[3]) && $args[3] > $var)))
					return;
			}
			break;
		case 'boolean':
			if(gettype($var) == 'string')
				$var = $var === 'true';
			if(gettype($var) == 'integer')
				$var = boolval($var);
			if(gettype($var) == 'boolean')
				return;
			break;
		case 'date':
			if(gettype($var) == 'string')
			{
				try
				{
					$d = new DateTime($var);
				}
				catch(Exception $e)
				{
					break;
				}
				if(isset($args[3]) && $d->format('Y-m-d') > $args[3]->format('Y-m-d'))
				{
					break;
				}
				if(isset($args[4]) && $d->format('Y-m-d') < $args[4]->format('Y-m-d'))
				{
					break;
				}
				$format = 'Y-m-d';
				if(isset($args[2]))
				{
					$format = $args[2];
				}
				$var = $d->format($format);
				return;
			}
			break;
		case 'json':
			if(gettype($var) == 'string')
			{
				try
				{
					$var = json_decode($var, true);
				}
				catch(Exception $e)
				{
					break;
				}
				return;
			}
			break;
		case 'array':
			if(gettype($var) == 'array')
			{
				if((empty($args[2]) || count($var) <= $args[2])
					&& (empty($args[3]) || count($var) >= $args[3]))
					return;
			}
			break;
		case 'file':
			if(gettype($var['type']) == 'string')
			{
				$count = count($args);
				if($count == 2)
					return;
				for($i = 2; $i < $count; ++$i)
					if($var['type'] == $args[$i])
						return;
			}
			break;
		case 'files':
			if(gettype($var['type']) == 'array')
			{
				$count = count($args);
				$count2 = count($var['type']);
				$all = true;
				for($j = 0; $j < $count2 && $all; ++$j)
					for($i = 2; $i < $count && $all; ++$i)
						$all = $var['type'][$j] == $args[$i];
				if($all) return;
			}
			break;
		case 'tel':
			if(gettype($var) == 'string')
			{
				if(preg_match('/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/', $var))
					return;
			}
			break;
		default:
			throw new Exception('Unknown variable type \''.$type.'\'');
	}
	wrong_param();
}

function missing_param()
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Hiányzó paraméterek',
	)));
}

function wrong_param()
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Hibás paraméterek',
	)));
}

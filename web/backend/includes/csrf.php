<?php

// CSRF token meglétének és hitelességének ellenőrzése
if(empty($_POST['csrf']) || empty($_SESSION['csrf']) || $_POST['csrf'] != $_SESSION['csrf'])
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'CSRF failure',
	)));

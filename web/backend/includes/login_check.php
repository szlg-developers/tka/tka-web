<?php

// Bejelentkezés ellenőrzése
session_start();
if(empty($_SESSION['user']))
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Nincs bejelentkezve',
	)));
}
include __DIR__.'/../../../config.php';
$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, 'tka');

function login_check(int $level, int $school_id = 0)
{
	// Bejelentkezés hitelességének ellenőrzése
	global $conn;
	$result = $conn->query('
		SELECT `users`.`username`, `users`.`name`, `users`.`password`, `users`.`level`, `schools`.`id` AS `school_id`, `schools`.`code` AS `school` FROM `users`
		LEFT JOIN `schools` ON `schools`.`id` = `users`.`school_id`
		WHERE `users`.`id` = '.$_SESSION['user']['id'].'
	');
	if($result->num_rows == 0)
	{
		$result->close();
		$conn->close();
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'A fiókját törölték',
		)));
	}
	$user_row = $result->fetch_assoc();
	$result->close();
	if($user_row['username'] != $_SESSION['user']['username'] || $user_row['name'] != $_SESSION['user']['name'] || $user_row['password'] != $_SESSION['user']['password'] || $user_row['level'] != $_SESSION['user']['level'] || $user_row['school'] != $_SESSION['user']['school'])
	{
		$conn->close();
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Nincs bejelentkezve',
		)));
	}
	if($level > $_SESSION['user']['level'] || $_SESSION['user']['level'] != 4 && $user_row['school_id'] != $school_id)
	{
		$conn->close();
		exit(json_encode(array
		(
			'success' => false,
			'message' => 'Hozzáférés megtagadva',
		)));
	}
}

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(4);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'id');
check_param($_POST['id'], 'integer');

// Rendszergazda felhasználó adatainak lekérése
$result = $conn->query('SELECT `username`, `name` FROM `users` WHERE `level` = 4 AND `id` = '.$_POST['id']);
if(!($row = $result->fetch_assoc()))
{
	$result->close();
	$conn->close();
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'A kért felhasználó nem található',
	)));
}
$result->close();

echo json_encode(array
(
	'success' => true,
	'data' => array
	(
		'id' => $_POST['id'],
		'username' => $row['username'],
		'name' => $row['name'],
	),
));

$conn->close();

<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(4);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'username', 'password');
check_param($_POST['username'], 'string', 255, 1, true);
check_param($_POST['name'], 'string', 255, 0, true);
check_param($_POST['password'], 'string', null, 1);

// Jelszó titkosítása
$_POST['password'] = password_hash($_POST['password'], PASSWORD_BCRYPT);

// Rendszergazda felhasználó hozzáadása
$query = $conn->prepare('INSERT INTO `users` (`school_id`, `username`, `name`, `password`, `level`, `date`) VALUES (1, ?, ?, ?, 4, NOW())');
$query->bind_param('sss', $_POST['username'], $_POST['name'], $_POST['password']);
$query->execute();
$query->close();

echo json_encode(array
(
	'success' => true,
	'valid' => $conn->insert_id != 0,
));

$conn->close();

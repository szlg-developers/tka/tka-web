<?php

header('Content-type: application/json');
include '../includes/login_check.php'; login_check(4);
include '../includes/csrf.php';
include '../includes/parameter_check.php';

isset_param($_POST, 'id');
check_param($_POST['id'], 'integer');

// Saját fiók törlésének tiltása
if($_POST['id'] == $_SESSION['user']['id'])
{
	exit(json_encode(array
	(
		'success' => false,
		'message' => 'Saját fiókot nem lehet törölni',
	)));
}

// Rendszergazda felhasználó törlése
$conn->query('DELETE FROM `users` WHERE `id` = '.$_POST['id']);

echo json_encode(array
(
	'success' => true,
));

$conn->close();

# TKA Web

Ez a **Telefonos Kölcsönzéskezelő Alkalmazás** adatbázisának és webes felületének repository-ja.

- [Tudnivalók](#tudnivalók)
- [Telepítés](#telepítés)
    - [1. Szerveres környezet előkészítése](#1-szerveres-környezet-előkészítése)
        - [1.1 Csomagok telepítése](#11-csomagok-telepítése)
        - [1.2 apache2 beállítása](#12-apache2-beállítása)
        - [1.3 PHP beállítása](#13-php-beállítása)
        - [1.4 MySQL szerver beállítása](#14-mysql-szerver-beállítása)
    - [2. Weboldal és adatbázis beállítása](#2-weboldal-és-adatbázis-beállítása)
        - [Git-en keresztüli beállítás](#git-en-keresztüli-beállítás)
        - [Beállítás forráskódból](#beállítás-forráskódból)
- [Frissítés](#frissítés)
    - [Git-en keresztüli frissítés](#git-en-keresztüli-frissítés)
    - [Git nélküli frissítés](#git-nélküli-frissítés)
- [Támogatás, közremukodes](#támogatás-közreműködés)

## Tudnivalók

A projekt jelenleg **fejlesztési fázisban** van, csak **saját felelősségre** használja!

Licencszerződés: [GNU AGPLv3](LICENSE)

**Fontos:** A lejjebb található telepítési és frissítési leírás **csak iránymutatás**, rendszergazdai ismeretek nélkül **semmiképp se telepítse** a szoftvert!

## Telepítés

### 1. Szerveres környezet előkészítése

#### 1.1 Csomagok telepítése

Győződjön meg arról, hogy a szerverén fel vannak telepítve, és megfelelően be vannak állítva a következő csomagok:

- **apache2** Más webszerverrel nem kompatibilis! (.htaccess fájl szükségessége miatt)
- **php7.3** vagy frissebb verzió
- **mariadb-server** vagy **mysql-server**
- **git** Opcionális: az oldal beállításához és frissítéséhez

Debian alapú rendszeren ezeket a következő parancscsal lehet feltelepíteni:
```sh
sudo apt update && sudo apt install apache2 php mariadb-server
```

#### 1.2 apache2 beállítása

- engedélyezze a .htaccess fájlok használatát a configban (`AllowOverride All`)
- engedélyezze a **rewrite** modult (`a2enmod rewrite`)
- HTTPS használata a biztonság érdekében erősen ajánlott!

#### 1.3 PHP beállítása

Telepítse és engedélyezze a következő kiterjesztéseket:
- **mysqli**
- **mbstring**
- **curl**
- **openssl**
- **gd2**

Debian alapú rendszeren:
```sh
sudo apt update && sudo apt install php-mysqli php-mbstring php-curl php-openssl php-gd2
```

#### 1.4 MySQL szerver beállítása

Állítson be egy felhasználót az oldal számára (a `felhasznalonev` és `jelszo` értékeket cserélje ki):

```sh
sudo mysql
```
```sql
> CREATE USER 'felhasznalonev'@'localhost' IDENTIFIED BY 'jelszo';
> GRANT ALL PRIVILEGES ON `tka%`.* TO 'felhasznalonev'@'localhost';
```

### 2. Weboldal és adatbázis beállítása

#### Git-en keresztüli beállítás

1. Git repository klónolása a `/var/www/html/` mappába

    ```sh
    sudo git clone https://gitlab.com/szlg-developers/tka/tka-web.git /var/www/html/
    ```

2. `migrate.sh` lefuttatása az adatbázis beállításához és a konfigurációs értékek megadásához

    ```sh
    cd /var/www/html/
    sudo ./migrate.sh
    ```

3. Mappa-jogosultságok beállítása

    ```sh
    sudo chown -R www-data:www-data /var/www/html/
    ```

#### Beállítás forráskódból

Egyelőre nem támogatott.

## Frissítés

### Git-en keresztüli frissítés

1. Webszerver leállítása (`sudo apachectl graceful-stop`)

2. Oldal frissítése

    ```sh
    cd /var/www/html/
    sudo git fetch && sudo git pull
    sudo ./migrate.sh
    sudo chown -R www-data:www-data .
    ```

3. Webszerver elindítása (`sudo systemctl start apache2`)

### Git nélküli frissítés

Egyelőre nem támogatott.

## Támogatás, közreműködés

A projekt fennmaradásának érdekében szívesen fogadunk bármi nemű támogatást vagy segítséget.

Kérdés esetén bátran keressen meg minket!

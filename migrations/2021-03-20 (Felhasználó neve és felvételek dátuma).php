<?php

$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, 'tka');

echo 'Adatbázis frissítése...'.PHP_EOL;

mm_multi_query($conn, "
	ALTER TABLE `users` ADD COLUMN `name` varchar(255);
	UPDATE `users` SET `name` = `username`;
	ALTER TABLE `users` MODIFY COLUMN `name` varchar(255) NOT NULL;

	ALTER TABLE `users` ADD COLUMN `date` datetime;
	UPDATE `users` SET `date` = NOW();
	ALTER TABLE `users` MODIFY COLUMN `date` datetime NOT NULL;

	ALTER TABLE `appkeys` ADD COLUMN `date` datetime;
	UPDATE `appkeys` SET `date` = NOW();
	ALTER TABLE `appkeys` MODIFY COLUMN `date` datetime NOT NULL;

	ALTER TABLE `schools` ADD COLUMN `date` datetime;
	UPDATE `schools` SET `date` = NOW();
	ALTER TABLE `schools` MODIFY COLUMN `date` datetime NOT NULL;

	ALTER TABLE `classes` ADD COLUMN `date` datetime;
	UPDATE `classes` SET `date` = NOW();
	ALTER TABLE `classes` MODIFY COLUMN `date` datetime NOT NULL;

	ALTER TABLE `students` ADD COLUMN `date` datetime;
	UPDATE `students` SET `date` = NOW();
	ALTER TABLE `students` MODIFY COLUMN `date` datetime NOT NULL;

	ALTER TABLE `bookcategories` ADD COLUMN `date` datetime;
	UPDATE `bookcategories` SET `date` = NOW();
	ALTER TABLE `bookcategories` MODIFY COLUMN `date` datetime NOT NULL;

	ALTER TABLE `booktypes` ADD COLUMN `date` datetime;
	UPDATE `booktypes` SET `date` = NOW();
	ALTER TABLE `booktypes` MODIFY COLUMN `date` datetime NOT NULL;

	ALTER TABLE `books` ADD COLUMN `date` datetime;
	UPDATE `books` SET `date` = NOW();
	ALTER TABLE `books` MODIFY COLUMN `date` datetime NOT NULL;
");

$conn->close();

echo PHP_EOL;

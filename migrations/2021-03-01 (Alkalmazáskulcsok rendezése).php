<?php

$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, 'tka');

echo 'Adatbázis frissítése...'.PHP_EOL;

mm_multi_query($conn, "
	ALTER TABLE `appkeys` ADD COLUMN `regist_token` varchar(32) NOT NULL UNIQUE AFTER `school_id`;
	ALTER TABLE `appkeys` MODIFY COLUMN `key` varchar(256) NOT NULL UNIQUE;
	ALTER TABLE `appkeys` ADD COLUMN `registered` boolean NOT NULL DEFAULT 0 AFTER `regist_token`;
	ALTER TABLE `appkeys` ADD COLUMN `enabled` boolean NOT NULL DEFAULT 1 AFTER `registered`;
");

mm_add_config('INSTANCE_NAME', 'Nincs megadva', 'Tanker/szervezet neve');

$conn->close();

echo PHP_EOL;

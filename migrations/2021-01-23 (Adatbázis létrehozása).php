<?php

$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD);

echo 'Adatbázis létrehozása...'.PHP_EOL;

mm_multi_query($conn, "
	CREATE DATABASE `tka`;
	USE `tka`;

	CREATE TABLE `schools`
	(
		`id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
		`code` varchar(32) NOT NULL UNIQUE,
		`name` varchar(255) NOT NULL
	);

	CREATE TABLE `users`
	(
		`id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
		`username` varchar(255) NOT NULL,
		`password` varchar(60) NOT NULL,
		`school_id` int UNSIGNED NOT NULL,
		FOREIGN KEY (`school_id`) REFERENCES `schools`(`id`) ON DELETE CASCADE,
		`level` tinyint NOT NULL DEFAULT 1 CHECK(`level` IN (1, 2, 3, 4)),
		UNIQUE(`username`, `school_id`),
		CHECK(IF(`school_id` <> 1, `level` <> 4, `level` = 4) = 1)
	);
	CREATE TABLE `appkeys`
	(
		`id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
		`key` varchar(32) NOT NULL UNIQUE,
		`name` varchar(255) NOT NULL DEFAULT 'Névtelen alkalmazáskulcs',
		`school_id` int UNSIGNED NOT NULL,
		FOREIGN KEY (`school_id`) REFERENCES `schools`(`id`) ON DELETE CASCADE,
		`lastused` datetime
	);

	CREATE TABLE `classes`
	(
		`id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
		`name` varchar(255) NOT NULL,
		`school_id` int UNSIGNED NOT NULL,
		FOREIGN KEY (`school_id`) REFERENCES `schools`(`id`) ON DELETE CASCADE
	);
	CREATE TABLE `students`
	(
		`id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
		`omaz` varchar(11) NOT NULL UNIQUE,
		`class_id` int UNSIGNED NOT NULL,
		FOREIGN KEY (`class_id`) REFERENCES `classes`(`id`) ON DELETE CASCADE,
		`name` varchar(255) NOT NULL
	);

	CREATE TABLE `bookcategories`
	(
		`id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
		`name` varchar(255) NOT NULL,
		`school_id` int UNSIGNED NOT NULL,
		FOREIGN KEY (`school_id`) REFERENCES `schools`(`id`) ON DELETE CASCADE
	);
	CREATE TABLE `booktypes`
	(
		`id` int UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
		`category_id` int UNSIGNED NOT NULL,
		FOREIGN KEY (`category_id`) REFERENCES `bookcategories`(`id`) ON DELETE CASCADE,
		`title` varchar(255) NOT NULL
	);
	CREATE TABLE `books`
	(
		`code` varchar(12) NOT NULL PRIMARY KEY,
		`type_id` int UNSIGNED NOT NULL,
		FOREIGN KEY (`type_id`) REFERENCES `booktypes`(`id`) ON DELETE CASCADE
	);

	CREATE TABLE `rentals`
	(
		`book_code` varchar(12) NOT NULL PRIMARY KEY,
		FOREIGN KEY (`book_code`) REFERENCES `books`(`code`) ON DELETE CASCADE,
		`student_id` int UNSIGNED NOT NULL,
		FOREIGN KEY (`student_id`) REFERENCES `students`(`id`),
		`date` datetime NOT NULL
	);
");

$conn->query("INSERT INTO `schools` (`id`, `name`, `code`) VALUES (1, 'Rendszergazda', 'rendszergazda')");
$conn->query("INSERT INTO `users` (`id`, `username`, `password`, `school_id`, `level`) VALUES (1, 'admin', '".password_hash('admin', PASSWORD_BCRYPT)."', 1, 4)");

$valid = false;
do
{
	$answer = mm_readline('Oldal elérési útja (pl. https://tka.mydomain.hu/ vagy http://127.0.0.1/tka-web/web/): ');
	if(!filter_var($answer, FILTER_VALIDATE_URL))
		echo 'Érvénytelen URL'.PHP_EOL;
	else
		$valid = true;
}
while(!$valid);

if($answer[strlen($answer) - 1] == '/')
	$answer = substr($answer, 0, strlen($answer) - 1);

mm_add_config('ROOT_URL', $answer, '', false, false, 'if you want to change this, modify .htaccess too');

$location = implode('/', array_slice(explode('/', $answer), 3));
$location = ($location ? '/' : '').$location;

file_put_contents(__DIR__.'/../web/.htaccess', <<<EOF
<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteCond %{REQUEST_URI} !^$location/assets/.*\$ [NC]
	RewriteCond %{REQUEST_URI} !^$location/backend/.*\$ [NC]
	RewriteCond %{REQUEST_URI} !^$location/favicon.ico\$ [NC]
	RewriteRule ^(.*)\$ index.php?url=\$1 [QSA,L]
</IfModule>

EOF
);

mm_add_config('WEB_ROOT', $location, '', false, false, 'if you want to change this, modify .htaccess too');

$conn->close();

echo PHP_EOL;

<?php

$conn = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, 'tka');

echo 'Adatbázis frissítése...'.PHP_EOL;

mm_multi_query($conn, "
	ALTER TABLE `books` MODIFY COLUMN `code` varchar(32) NOT NULL;
");

$conn->close();

echo PHP_EOL;
